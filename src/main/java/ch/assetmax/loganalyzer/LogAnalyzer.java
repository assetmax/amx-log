package ch.assetmax.loganalyzer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

enum FixDirection {
  Send("->"),
  Receive("<-");

  private final String _directionText;

  FixDirection(String directionText) {
    _directionText = directionText;
  }

  String toDirectionText() {
    return _directionText;
  }
}

public class LogAnalyzer {

  private static final Pattern PATTERN_CALL = Pattern.compile("\\[([A-Z0-9-]*)\\] == \\[([A-Za-z]*)\\] +(.*)$");
  private static final Pattern PATTERN_ASYNC_REQUEST = Pattern.compile("\\[AsyncCallService\\] +ASYNC REQUEST +([^ ]+) *([^ ]*)$");
  private static final Pattern PATTERN_ASYNC_DONE = Pattern.compile("\\[AsyncCallService\\] +ASYNC DONE +([^ ]+) *([^ ]*)$");
  private static final Pattern PATTERN_FIX = Pattern.compile("message: (.*)$");

  private static final List<String> DEFAULT_INTERESTING_FIX_KEYS = Arrays.asList("MsgType", "OrdStatus", "Side", "SecurityID", "OrderQty", "Quantity", "LastQty", "CumQty");

  public static void main(String[] args) {
    boolean analyzeFix = false;
    boolean analyzeCalls = false;
    boolean showCallParams = false;

    List<String> interestingFixKeys = DEFAULT_INTERESTING_FIX_KEYS;
    List<String> files = new ArrayList<>();

    for (int argIndex = 0; argIndex < args.length; argIndex++) {
      String arg = args[argIndex];
      switch (arg) {
        case "--fix" :
          analyzeFix = true;
          break;
        case "--fix-keys" :
          argIndex++;
          interestingFixKeys = Arrays.asList(args[argIndex].split(Pattern.quote(",")));
          break;
        case "--fix-protocol" :
          printFix();
          System.exit(0);
          break;
        case "--call" :
          analyzeCalls = true;
          break;
        case "--call-params" :
          showCallParams = true;
          break;
        case "-h" :
        case "-?" :
        case "--help" :
          printHelp();
          System.exit(0);
          break;
        default :
          files.add(arg);
      }
    }

    if (!analyzeFix && !analyzeCalls) {
      analyzeFix = true;
      analyzeCalls = true;
    }

    analyzeLogs(analyzeCalls, analyzeFix, showCallParams, interestingFixKeys, files);
  }

  private static void printHelp() {
    String defaultFixKeys = DEFAULT_INTERESTING_FIX_KEYS.stream().collect(Collectors.joining(","));

    System.out.println(""
        + "USAGE: amxlog [options] [FILE]...\n"
        + "  Converts Assetmax log files for simplified analysis.\n"
        + "\n"
        + "ARGUMENTS:\n"
        + "  FILE Assetmax log file to convert\n"
        + "\n"
        + "OPTIONS:\n"
        + "  --fix\n"
        + "    Extracts FIX messages from the log files and converts them into a readable format.\n"
        + "\n"
        + "  --call\n"
        + "    Extracts call request information (from client or API) and aggregates them into per-call information.\n"
        + "\n"
        + "  --fix-keys KEYLIST\n"
        + "    In --fix mode a simplified message containing only specific FIX field values is printed.\n"
        + "    The KEYLIST is a list of FIX field keys (as code or mapped into name), separated by ','.\n"
        + "    Default: " + defaultFixKeys + "\n"
        + "\n"
        + "  --call-params\n"
        + "    In --call mode show the parameters of the every call request (synchronous or asynchronous)."
        + "\n"
        + "  --fix-protocol\n"
        + "    Prints the information about all known FIX fields and their values.\n"
        + "\n"
        + "  -?\n"
        + "  -h\n"
        + "  --help\n"
        + "    Prints this help text.\n"
        + "\n"
        + "OUTPUT:\n"
        + "  Mode --fix:\n"
        + "    Every output line represents a single FIX message and consists of:\n"
        + "      - date\n"
        + "      - time\n"
        + "      - 'Fix'\n"
        + "      - '[' orderID ']'\n"
        + "      - '[' simplifiedMessage ']'\n"
        + "    and many repeated\n"
        + "      - '| ' field '=' value\n"
        + "\n"
        + "    The `date` and `time` are in the same format as in the original log file.\n"
        + "    The `orderId` is useful as correlation ID and consists of the OrigClOrdID value, or ClOrdID value.\n"
        + "\n"
        + "    The `simplifiedMessage` consists of the values of a limited list of important fields. \n"
        + "    This output is useful to gain a quick overview over the FIX message.\n"
        + "    If not specified otherwise with --fix-keys these\n"
        + "    fields are: " + defaultFixKeys + "\n"
        + "\n"
        + "    Fields and values are expanded according to FIX protocol.\n"
        + "    For example '35=8' is converted into 'MsgType(35)=Execution Report(8)'.\n"
        + "    If no field information is available, the original field or its original value is printed.\n"
        + "\n"
        + "  Mode --call:\n"
        + "    Every output line represents a single call request and consists of:\n"
        + "      - date\n"
        + "      - time\n"
        + "      - 'Call'\n"
        + "      - URL\n"
        + "      - elapsedTime\n"
        + "      - 'SYNC' or 'ASYNC'\n"
        + "      - params\n"
        + "\n"
        + "    The `date` and `time` are in the same format as in the original log file.\n"
        + "    The `URL` is the URL of the call request.\n"
        + "    The `elapsedTime` is the elapsed time of the call execution in milliseconds.\n"
        + "    The `params` are the call request parameters, only printed if the --call-params options was specified.\n"
        + "\n"
        + "EXAMPLES:\n"
        + "  Usually the Assetmax log files are initially converted using --fix or -- call modes.\n"
        + "  The resulting output file can then be further analyzed using `grep`, `sort` and viewed with `less -S`.\n"
        + "\n"
        + "  Mode --fix:\n"
        + "    Convert the log file(s) using:\n"
        + "      amxlog --fix application*.log >fix.txt\n"
        + "    Find all messages (may be multiple orders) for a specific ISING CH0418792922:\n"
        + "      grep CH0025238863 fix.txt | less -S\n"
        + "    Output:\n"
        + "      11.08.21 13:54:38.535 Fix [XY22031792401] [New Order Single Buy CH0418792922 102] | BeginString(8)=FIX.4.4 ...\n"
        + "      11.08.21 13:56:46.010 Fix [XY22031792401] [Execution Report Rejected Buy CH0418792922] | BeginString(8)=FI ...\n"
        + "      11.08.21 14:04:01.583 Fix [XY22031742128] [New Order Single Sell CH0418792922 8] | BeginString(8)=FIX.4.4  ...\n"
        + "      11.08.21 14:04:16.209 Fix [XY22031742128] [Execution Report Pending New Sell CH0418792922 8] | BeginString ...\n"
        + "      11.08.21 14:06:36.331 Fix [XY22031742128] [Execution Report New Sell CH0418792922 8] | BeginString(8)=FIX. ...\n"
        + "      11.08.21 14:06:47.301 Fix [XY22031742128] [Execution Report Filled Sell CH0418792922 8 8] | BeginString(8) ...\n"
        + "      11.08.21 14:06:51.207 Fix [XY22031742128] [Allocation Instruction CH0418792922 8 Sell 8] | BeginString(8)= ...\n"
        + "    Find all messages for a specific correlation order ID (first column after 'Fix') XY22031792401:\n"
        + "      grep XY22031716682 fix.txt | less -S\n"
        + "    Output:\n"
        + "      11.08.21 13:54:38.535 Fix [XY22031792401] [New Order Single Buy CH0418792922 102] | BeginString(8)=FIX.4.4 ...\n"
        + "      11.08.21 13:56:46.010 Fix [XY22031792401] [Execution Report Rejected Buy CH0418792922] | BeginString(8)=FI ...\n"
        + "\n"
        + "  Mode --call:\n"
        + "    Convert the log file(s) using:\n"
        + "      amxlog --call application*.log >call.txt\n"
        + "    or:\n"
        + "      amxlog --call --call-params application*.log >call.txt\n"
        + "    Find all calls of 'pf-operation/add-op-update-breaches' sorted by descending elapsed time:\n"
        + "      grep 'pf-operation/add-op-update-breaches' call.txt | sort -k 5 -n -r\n"
        + "\n");
  }

  static void printFix() {
    for (String fieldCode : FixProtocol.MAP_FIELD_CODE_TO_NAME.keySet()) {
      String fieldCodeName = FixProtocol.mapFieldCode(fieldCode);
      Map<String, String> fieldValuesMap = FixProtocol.MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.get(fieldCode);
      System.out.println(fieldCode + " : " + fieldCodeName);
      if (fieldValuesMap != null) {
        for (String fieldValueCode : fieldValuesMap.keySet()) {
          System.out.println("  " + fieldValueCode + " : " + fieldValuesMap.get(fieldValueCode));
        }
      }
    }
  }

  public static void analyzeLogs(boolean analyzeCalls, boolean analyzeFix, boolean showCallParams, List<String> interestingFixKeys, List<String> fileNames) {
    Map<String, Map<String, String>> mapCallInfo = new HashMap<>();
    Map<String, String> mapToCallId = new HashMap<>();

    for (String fileName : fileNames) {
      File file = new File(fileName);

      try {
        BufferedReader reader = new BufferedReader(new FileReader(file));
        String line = reader.readLine();
        while (line != null) {
          if (analyzeCalls) {
            analyzeCall(mapCallInfo, mapToCallId, line, showCallParams);
          }

          if (analyzeFix) {
            if (line.contains("FixClientApplication.toApp") || line.contains("FixLinkApplication.toApp")) {
              analyzeFixMessage(line, FixDirection.Send, interestingFixKeys);
            }
            if (line.contains("FixClientApplication.fromApp") || line.contains("FixLinkApplication.fromApp")) {
              analyzeFixMessage(line, FixDirection.Receive, interestingFixKeys);
            }
          }

          line = reader.readLine();
        }
      }
      catch (FileNotFoundException e) {
        e.printStackTrace();
      }
      catch (IOException e) {
        e.printStackTrace();
      }
    }
  }

  private static void analyzeCall(Map<String, Map<String, String>> mapCallInfo, Map<String, String> mapToCallId, String line, boolean showCallParams) {
    String[] splitLine = line.split(" +");
    String date = splitLine.length > 0 ? splitLine[0] : null;
    String time = splitLine.length > 1 ? splitLine[1] : null;

    Matcher matcherCall = PATTERN_CALL.matcher(line);
    if (matcherCall.find()) {
      String callId = matcherCall.group(1);
      String name = matcherCall.group(2);
      String value = matcherCall.group(3);
      Map<String, String> info = mapCallInfo.get(callId);
      if (info == null) {
        info = new HashMap<>();
        mapCallInfo.put(callId, info);
      }
      info.put(name, value);

      if ("URL".equals(name)) {
        info.put("$date", date);
        info.put("$time", time);
      }
      if ("Thread".equals(name)) {
        info.put("$thread", value);
        mapToCallId.put(value, callId);
      }
      if ("Param".equals(name)) {
        if (value.contains("_async_=true")) {
          info.put("$async", "true");
        }
      }
      if ("Done".equals(name)) {
        boolean async = "true".equals(info.get("$async"));
        if (!async) {
          String elapsedMillis = info.get("Done");
          reportCallDone(info, showCallParams, false, elapsedMillis);
          mapCallInfo.remove(callId);
          mapToCallId.remove(info.get("$thread"));
        }
      }
      return;
    }

    Matcher matcherAsyncRequest = PATTERN_ASYNC_REQUEST.matcher(line);
    if (matcherAsyncRequest.find()) {
      String callDescriptionId = matcherAsyncRequest.group(1);
      String threadName = matcherAsyncRequest.group(2);

      String callId = mapToCallId.get(threadName);
      if (callId != null) {
        mapToCallId.put(callDescriptionId, callId);
      }
      return;
    }

    Matcher matcherAsyncDone = PATTERN_ASYNC_DONE.matcher(line);
    if (matcherAsyncDone.find()) {
      String callDescriptionId = matcherAsyncDone.group(1);
      String elapsedMillis = matcherAsyncDone.group(2);

      String callId = mapToCallId.remove(callDescriptionId);
      if (callId != null) {
        Map<String, String> info = mapCallInfo.get(callId);
        reportCallDone(info, showCallParams, true, elapsedMillis);
        mapCallInfo.remove(callId);
      }
    }
  }

  private static void reportCallDone(
      Map<String, String> info,
      boolean showCallParams,
      boolean async,
      String elapsedMillis
  ) {
    String syncType = async ? "ASYNC" : "SYNC";
    if (elapsedMillis.endsWith("ms")) {
      elapsedMillis = elapsedMillis.substring(0, elapsedMillis.length() - "ms".length());
    }
    System.out.print(String.format("%8s %12s Call %-120s %10s %5s", info.get("$date"), info.get("$time"), info.get("URL"), elapsedMillis, syncType));
    if (showCallParams) {
      System.out.print(" " + info.get("Param"));
    }
    System.out.println();
  }

  private static void analyzeFixMessage(String line, FixDirection fixDirection, List<String> interestingFixKeys) {
    Matcher matcherFix = PATTERN_FIX.matcher(line);
    if (matcherFix.find()) {
      String[] splitLine = line.split(" +");
      String date = splitLine[0];
      String time = splitLine[1];

      String message = matcherFix.group(1);
      String[] fields = message.split(Pattern.quote("\u0001"));
      String orderId = "";
      String originalOrderId = null;

      Map<String, String> mapFieldToValue = new HashMap<>();
      StringBuilder logMessage = new StringBuilder();

      for (String fieldAssignment : fields) {
        int indexAssignment = fieldAssignment.indexOf("=");
        if (indexAssignment > 0) {
          String field = fieldAssignment.substring(0, indexAssignment);
          String value = fieldAssignment.substring(indexAssignment + 1);
          String fieldName = FixProtocol.mapFieldCode(field);
          String valueName = FixProtocol.mapFieldValueCode(field, value);

          mapFieldToValue.put(field, valueName);
          mapFieldToValue.put(fieldName, valueName);

          switch (fieldName) {
            case "ClOrdID" :
              orderId = valueName;
              break;
            case "OrigClOrdID" :
              originalOrderId = valueName;
              break;
         }

          if (field.equals(fieldName)) {
            logMessage.append(" | " + fieldName + "=");
          } else {
            logMessage.append(" | " + fieldName + "(" + field + ")=");
          }
          if (value.equals(valueName)) {
            logMessage.append(valueName);
          } else {
            logMessage.append(valueName + "(" + value + ")");
          }
        }
      }

      if (originalOrderId != null) {
        orderId = originalOrderId;
      }

      StringBuilder valuesMessage = new StringBuilder();
      for (String interestingFixKey : interestingFixKeys) {
          if (mapFieldToValue.get(interestingFixKey) != null) {
            if (valuesMessage.length() > 0) {
              valuesMessage.append(" ");
            }
            valuesMessage.append(mapFieldToValue.get(interestingFixKey));
          }
      }

      System.out.println(String.format("%8s %12s Fix %s [%s] [%s]%s", date, time, fixDirection.toDirectionText(), orderId, valuesMessage, logMessage));
    }
  }
}
