package ch.assetmax.loganalyzer;

import java.util.LinkedHashMap;
import java.util.Map;

public class FixProtocol {

  public static final Map<String, String> MAP_FIELD_CODE_TO_NAME = new LinkedHashMap<>();
  static {
    MAP_FIELD_CODE_TO_NAME.put("1","Account");
    MAP_FIELD_CODE_TO_NAME.put("2","AdvId");
    MAP_FIELD_CODE_TO_NAME.put("3","AdvRefID");
    MAP_FIELD_CODE_TO_NAME.put("4","AdvSide");
    MAP_FIELD_CODE_TO_NAME.put("5","AdvTransType");
    MAP_FIELD_CODE_TO_NAME.put("6","AvgPx");
    MAP_FIELD_CODE_TO_NAME.put("7","BeginSeqNo");
    MAP_FIELD_CODE_TO_NAME.put("8","BeginString");
    MAP_FIELD_CODE_TO_NAME.put("9","BodyLength");
    MAP_FIELD_CODE_TO_NAME.put("10","CheckSum");
    MAP_FIELD_CODE_TO_NAME.put("11","ClOrdID");
    MAP_FIELD_CODE_TO_NAME.put("12","Commission");
    MAP_FIELD_CODE_TO_NAME.put("13","CommType");
    MAP_FIELD_CODE_TO_NAME.put("14","CumQty");
    MAP_FIELD_CODE_TO_NAME.put("15","Currency");
    MAP_FIELD_CODE_TO_NAME.put("16","EndSeqNo");
    MAP_FIELD_CODE_TO_NAME.put("17","ExecID");
    MAP_FIELD_CODE_TO_NAME.put("18","ExecInst");
    MAP_FIELD_CODE_TO_NAME.put("19","ExecRefID");
    MAP_FIELD_CODE_TO_NAME.put("20","ExecTransType");
    MAP_FIELD_CODE_TO_NAME.put("21","HandlInst");
    MAP_FIELD_CODE_TO_NAME.put("22","SecurityIDSource");
    MAP_FIELD_CODE_TO_NAME.put("23","IOIID");
    MAP_FIELD_CODE_TO_NAME.put("24","IOIOthSvc");
    MAP_FIELD_CODE_TO_NAME.put("25","IOIQltyInd");
    MAP_FIELD_CODE_TO_NAME.put("26","IOIRefID");
    MAP_FIELD_CODE_TO_NAME.put("27","IOIQty");
    MAP_FIELD_CODE_TO_NAME.put("28","IOITransType");
    MAP_FIELD_CODE_TO_NAME.put("29","LastCapacity");
    MAP_FIELD_CODE_TO_NAME.put("30","LastMkt");
    MAP_FIELD_CODE_TO_NAME.put("31","LastPx");
    MAP_FIELD_CODE_TO_NAME.put("32","LastQty");
    MAP_FIELD_CODE_TO_NAME.put("33","NoLinesOfText");
    MAP_FIELD_CODE_TO_NAME.put("34","MsgSeqNum");
    MAP_FIELD_CODE_TO_NAME.put("35","MsgType");
    MAP_FIELD_CODE_TO_NAME.put("36","NewSeqNo");
    MAP_FIELD_CODE_TO_NAME.put("37","OrderID");
    MAP_FIELD_CODE_TO_NAME.put("38","OrderQty");
    MAP_FIELD_CODE_TO_NAME.put("39","OrdStatus");
    MAP_FIELD_CODE_TO_NAME.put("40","OrdType");
    MAP_FIELD_CODE_TO_NAME.put("41","OrigClOrdID");
    MAP_FIELD_CODE_TO_NAME.put("42","OrigTime");
    MAP_FIELD_CODE_TO_NAME.put("43","PossDupFlag");
    MAP_FIELD_CODE_TO_NAME.put("44","Price");
    MAP_FIELD_CODE_TO_NAME.put("45","RefSeqNum");
    MAP_FIELD_CODE_TO_NAME.put("46","RelatdSym");
    MAP_FIELD_CODE_TO_NAME.put("47","Rule80A");
    MAP_FIELD_CODE_TO_NAME.put("48","SecurityID");
    MAP_FIELD_CODE_TO_NAME.put("49","SenderCompID");
    MAP_FIELD_CODE_TO_NAME.put("50","SenderSubID");
    MAP_FIELD_CODE_TO_NAME.put("51","SendingDate");
    MAP_FIELD_CODE_TO_NAME.put("52","SendingTime");
    MAP_FIELD_CODE_TO_NAME.put("53","Quantity");
    MAP_FIELD_CODE_TO_NAME.put("54","Side");
    MAP_FIELD_CODE_TO_NAME.put("55","Symbol");
    MAP_FIELD_CODE_TO_NAME.put("56","TargetCompID");
    MAP_FIELD_CODE_TO_NAME.put("57","TargetSubID");
    MAP_FIELD_CODE_TO_NAME.put("58","Text");
    MAP_FIELD_CODE_TO_NAME.put("59","TimeInForce");
    MAP_FIELD_CODE_TO_NAME.put("60","TransactTime");
    MAP_FIELD_CODE_TO_NAME.put("61","Urgency");
    MAP_FIELD_CODE_TO_NAME.put("62","ValidUntilTime");
    MAP_FIELD_CODE_TO_NAME.put("63","SettlType");
    MAP_FIELD_CODE_TO_NAME.put("64","SettlDate");
    MAP_FIELD_CODE_TO_NAME.put("65","SymbolSfx");
    MAP_FIELD_CODE_TO_NAME.put("66","ListID");
    MAP_FIELD_CODE_TO_NAME.put("67","ListSeqNo");
    MAP_FIELD_CODE_TO_NAME.put("68","TotNoOrders");
    MAP_FIELD_CODE_TO_NAME.put("69","ListExecInst");
    MAP_FIELD_CODE_TO_NAME.put("70","AllocID");
    MAP_FIELD_CODE_TO_NAME.put("71","AllocTransType");
    MAP_FIELD_CODE_TO_NAME.put("72","RefAllocID");
    MAP_FIELD_CODE_TO_NAME.put("73","NoOrders");
    MAP_FIELD_CODE_TO_NAME.put("74","AvgPxPrecision");
    MAP_FIELD_CODE_TO_NAME.put("75","TradeDate");
    MAP_FIELD_CODE_TO_NAME.put("76","ExecBroker");
    MAP_FIELD_CODE_TO_NAME.put("77","PositionEffect");
    MAP_FIELD_CODE_TO_NAME.put("78","NoAllocs");
    MAP_FIELD_CODE_TO_NAME.put("79","AllocAccount");
    MAP_FIELD_CODE_TO_NAME.put("80","AllocQty");
    MAP_FIELD_CODE_TO_NAME.put("81","ProcessCode");
    MAP_FIELD_CODE_TO_NAME.put("82","NoRpts");
    MAP_FIELD_CODE_TO_NAME.put("83","RptSeq");
    MAP_FIELD_CODE_TO_NAME.put("84","CxlQty");
    MAP_FIELD_CODE_TO_NAME.put("85","NoDlvyInst");
    MAP_FIELD_CODE_TO_NAME.put("86","DlvyInst");
    MAP_FIELD_CODE_TO_NAME.put("87","AllocStatus");
    MAP_FIELD_CODE_TO_NAME.put("88","AllocRejCode");
    MAP_FIELD_CODE_TO_NAME.put("89","Signature");
    MAP_FIELD_CODE_TO_NAME.put("90","SecureDataLen");
    MAP_FIELD_CODE_TO_NAME.put("91","SecureData");
    MAP_FIELD_CODE_TO_NAME.put("92","BrokerOfCredit");
    MAP_FIELD_CODE_TO_NAME.put("93","SignatureLength");
    MAP_FIELD_CODE_TO_NAME.put("94","EmailType");
    MAP_FIELD_CODE_TO_NAME.put("95","RawDataLength");
    MAP_FIELD_CODE_TO_NAME.put("96","RawData");
    MAP_FIELD_CODE_TO_NAME.put("97","PossResend");
    MAP_FIELD_CODE_TO_NAME.put("98","EncryptMethod");
    MAP_FIELD_CODE_TO_NAME.put("99","StopPx");
    MAP_FIELD_CODE_TO_NAME.put("100","ExDestination");
    MAP_FIELD_CODE_TO_NAME.put("102","CxlRejReason");
    MAP_FIELD_CODE_TO_NAME.put("103","OrdRejReason");
    MAP_FIELD_CODE_TO_NAME.put("104","IOIQualifier");
    MAP_FIELD_CODE_TO_NAME.put("105","WaveNo");
    MAP_FIELD_CODE_TO_NAME.put("106","Issuer");
    MAP_FIELD_CODE_TO_NAME.put("107","SecurityDesc");
    MAP_FIELD_CODE_TO_NAME.put("108","HeartBtInt");
    MAP_FIELD_CODE_TO_NAME.put("109","ClientID");
    MAP_FIELD_CODE_TO_NAME.put("110","MinQty");
    MAP_FIELD_CODE_TO_NAME.put("111","MaxFloor");
    MAP_FIELD_CODE_TO_NAME.put("112","TestReqID");
    MAP_FIELD_CODE_TO_NAME.put("113","ReportToExch");
    MAP_FIELD_CODE_TO_NAME.put("114","LocateReqd");
    MAP_FIELD_CODE_TO_NAME.put("115","OnBehalfOfCompID");
    MAP_FIELD_CODE_TO_NAME.put("116","OnBehalfOfSubID");
    MAP_FIELD_CODE_TO_NAME.put("117","QuoteID");
    MAP_FIELD_CODE_TO_NAME.put("118","NetMoney");
    MAP_FIELD_CODE_TO_NAME.put("119","SettlCurrAmt");
    MAP_FIELD_CODE_TO_NAME.put("120","SettlCurrency");
    MAP_FIELD_CODE_TO_NAME.put("121","ForexReq");
    MAP_FIELD_CODE_TO_NAME.put("122","OrigSendingTime");
    MAP_FIELD_CODE_TO_NAME.put("123","GapFillFlag");
    MAP_FIELD_CODE_TO_NAME.put("124","NoExecs");
    MAP_FIELD_CODE_TO_NAME.put("125","CxlType");
    MAP_FIELD_CODE_TO_NAME.put("126","ExpireTime");
    MAP_FIELD_CODE_TO_NAME.put("127","DKReason");
    MAP_FIELD_CODE_TO_NAME.put("128","DeliverToCompID");
    MAP_FIELD_CODE_TO_NAME.put("129","DeliverToSubID");
    MAP_FIELD_CODE_TO_NAME.put("130","IOINaturalFlag");
    MAP_FIELD_CODE_TO_NAME.put("131","QuoteReqID");
    MAP_FIELD_CODE_TO_NAME.put("132","BidPx");
    MAP_FIELD_CODE_TO_NAME.put("133","OfferPx");
    MAP_FIELD_CODE_TO_NAME.put("134","BidSize");
    MAP_FIELD_CODE_TO_NAME.put("135","OfferSize");
    MAP_FIELD_CODE_TO_NAME.put("136","NoMiscFees");
    MAP_FIELD_CODE_TO_NAME.put("137","MiscFeeAmt");
    MAP_FIELD_CODE_TO_NAME.put("138","MiscFeeCurr");
    MAP_FIELD_CODE_TO_NAME.put("139","MiscFeeType");
    MAP_FIELD_CODE_TO_NAME.put("140","PrevClosePx");
    MAP_FIELD_CODE_TO_NAME.put("141","ResetSeqNumFlag");
    MAP_FIELD_CODE_TO_NAME.put("142","SenderLocationID");
    MAP_FIELD_CODE_TO_NAME.put("143","TargetLocationID");
    MAP_FIELD_CODE_TO_NAME.put("144","OnBehalfOfLocationID");
    MAP_FIELD_CODE_TO_NAME.put("145","DeliverToLocationID");
    MAP_FIELD_CODE_TO_NAME.put("146","NoRelatedSym");
    MAP_FIELD_CODE_TO_NAME.put("147","Subject");
    MAP_FIELD_CODE_TO_NAME.put("148","Headline");
    MAP_FIELD_CODE_TO_NAME.put("149","URLLink");
    MAP_FIELD_CODE_TO_NAME.put("150","ExecType");
    MAP_FIELD_CODE_TO_NAME.put("151","LeavesQty");
    MAP_FIELD_CODE_TO_NAME.put("152","CashOrderQty");
    MAP_FIELD_CODE_TO_NAME.put("153","AllocAvgPx");
    MAP_FIELD_CODE_TO_NAME.put("154","AllocNetMoney");
    MAP_FIELD_CODE_TO_NAME.put("155","SettlCurrFxRate");
    MAP_FIELD_CODE_TO_NAME.put("156","SettlCurrFxRateCalc");
    MAP_FIELD_CODE_TO_NAME.put("157","NumDaysInterest");
    MAP_FIELD_CODE_TO_NAME.put("158","AccruedInterestRate");
    MAP_FIELD_CODE_TO_NAME.put("159","AccruedInterestAmt");
    MAP_FIELD_CODE_TO_NAME.put("160","SettlInstMode");
    MAP_FIELD_CODE_TO_NAME.put("161","AllocText");
    MAP_FIELD_CODE_TO_NAME.put("162","SettlInstID");
    MAP_FIELD_CODE_TO_NAME.put("163","SettlInstTransType");
    MAP_FIELD_CODE_TO_NAME.put("164","EmailThreadID");
    MAP_FIELD_CODE_TO_NAME.put("165","SettlInstSource");
    MAP_FIELD_CODE_TO_NAME.put("166","SettlLocation");
    MAP_FIELD_CODE_TO_NAME.put("167","SecurityType");
    MAP_FIELD_CODE_TO_NAME.put("168","EffectiveTime");
    MAP_FIELD_CODE_TO_NAME.put("169","StandInstDbType");
    MAP_FIELD_CODE_TO_NAME.put("170","StandInstDbName");
    MAP_FIELD_CODE_TO_NAME.put("171","StandInstDbID");
    MAP_FIELD_CODE_TO_NAME.put("172","SettlDeliveryType");
    MAP_FIELD_CODE_TO_NAME.put("173","SettlDepositoryCode");
    MAP_FIELD_CODE_TO_NAME.put("174","SettlBrkrCode");
    MAP_FIELD_CODE_TO_NAME.put("175","SettlInstCode");
    MAP_FIELD_CODE_TO_NAME.put("176","SecuritySettlAgentName");
    MAP_FIELD_CODE_TO_NAME.put("177","SecuritySettlAgentCode");
    MAP_FIELD_CODE_TO_NAME.put("178","SecuritySettlAgentAcctNum");
    MAP_FIELD_CODE_TO_NAME.put("179","SecuritySettlAgentAcctName");
    MAP_FIELD_CODE_TO_NAME.put("180","SecuritySettlAgentContactName");
    MAP_FIELD_CODE_TO_NAME.put("181","SecuritySettlAgentContactPhone");
    MAP_FIELD_CODE_TO_NAME.put("182","CashSettlAgentName");
    MAP_FIELD_CODE_TO_NAME.put("183","CashSettlAgentCode");
    MAP_FIELD_CODE_TO_NAME.put("184","CashSettlAgentAcctNum");
    MAP_FIELD_CODE_TO_NAME.put("185","CashSettlAgentAcctName");
    MAP_FIELD_CODE_TO_NAME.put("186","CashSettlAgentContactName");
    MAP_FIELD_CODE_TO_NAME.put("187","CashSettlAgentContactPhone");
    MAP_FIELD_CODE_TO_NAME.put("188","BidSpotRate");
    MAP_FIELD_CODE_TO_NAME.put("189","BidForwardPoints");
    MAP_FIELD_CODE_TO_NAME.put("190","OfferSpotRate");
    MAP_FIELD_CODE_TO_NAME.put("191","OfferForwardPoints");
    MAP_FIELD_CODE_TO_NAME.put("192","OrderQty2");
    MAP_FIELD_CODE_TO_NAME.put("193","SettlDate2");
    MAP_FIELD_CODE_TO_NAME.put("194","LastSpotRate");
    MAP_FIELD_CODE_TO_NAME.put("195","LastForwardPoints");
    MAP_FIELD_CODE_TO_NAME.put("196","AllocLinkID");
    MAP_FIELD_CODE_TO_NAME.put("197","AllocLinkType");
    MAP_FIELD_CODE_TO_NAME.put("198","SecondaryOrderID");
    MAP_FIELD_CODE_TO_NAME.put("199","NoIOIQualifiers");
    MAP_FIELD_CODE_TO_NAME.put("200","MaturityMonthYear");
    MAP_FIELD_CODE_TO_NAME.put("201","PutOrCall");
    MAP_FIELD_CODE_TO_NAME.put("202","StrikePrice");
    MAP_FIELD_CODE_TO_NAME.put("203","CoveredOrUncovered");
    MAP_FIELD_CODE_TO_NAME.put("204","CustomerOrFirm");
    MAP_FIELD_CODE_TO_NAME.put("205","MaturityDay");
    MAP_FIELD_CODE_TO_NAME.put("206","OptAttribute");
    MAP_FIELD_CODE_TO_NAME.put("207","SecurityExchange");
    MAP_FIELD_CODE_TO_NAME.put("208","NotifyBrokerOfCredit");
    MAP_FIELD_CODE_TO_NAME.put("209","AllocHandlInst");
    MAP_FIELD_CODE_TO_NAME.put("210","MaxShow");
    MAP_FIELD_CODE_TO_NAME.put("211","PegOffsetValue");
    MAP_FIELD_CODE_TO_NAME.put("212","XmlDataLen");
    MAP_FIELD_CODE_TO_NAME.put("213","XmlData");
    MAP_FIELD_CODE_TO_NAME.put("214","SettlInstRefID");
    MAP_FIELD_CODE_TO_NAME.put("215","NoRoutingIDs");
    MAP_FIELD_CODE_TO_NAME.put("216","RoutingType");
    MAP_FIELD_CODE_TO_NAME.put("217","RoutingID");
    MAP_FIELD_CODE_TO_NAME.put("218","Spread");
    MAP_FIELD_CODE_TO_NAME.put("219","Benchmark");
    MAP_FIELD_CODE_TO_NAME.put("220","BenchmarkCurveCurrency");
    MAP_FIELD_CODE_TO_NAME.put("221","BenchmarkCurveName");
    MAP_FIELD_CODE_TO_NAME.put("222","BenchmarkCurvePoint");
    MAP_FIELD_CODE_TO_NAME.put("223","CouponRate");
    MAP_FIELD_CODE_TO_NAME.put("224","CouponPaymentDate");
    MAP_FIELD_CODE_TO_NAME.put("225","IssueDate");
    MAP_FIELD_CODE_TO_NAME.put("226","RepurchaseTerm");
    MAP_FIELD_CODE_TO_NAME.put("227","RepurchaseRate");
    MAP_FIELD_CODE_TO_NAME.put("228","Factor");
    MAP_FIELD_CODE_TO_NAME.put("229","TradeOriginationDate");
    MAP_FIELD_CODE_TO_NAME.put("230","ExDate");
    MAP_FIELD_CODE_TO_NAME.put("231","ContractMultiplier");
    MAP_FIELD_CODE_TO_NAME.put("232","NoStipulations");
    MAP_FIELD_CODE_TO_NAME.put("233","StipulationType");
    MAP_FIELD_CODE_TO_NAME.put("234","StipulationValue");
    MAP_FIELD_CODE_TO_NAME.put("235","YieldType");
    MAP_FIELD_CODE_TO_NAME.put("236","Yield");
    MAP_FIELD_CODE_TO_NAME.put("237","TotalTakedown");
    MAP_FIELD_CODE_TO_NAME.put("238","Concession");
    MAP_FIELD_CODE_TO_NAME.put("239","RepoCollateralSecurityType");
    MAP_FIELD_CODE_TO_NAME.put("240","RedemptionDate");
    MAP_FIELD_CODE_TO_NAME.put("241","UnderlyingCouponPaymentDate");
    MAP_FIELD_CODE_TO_NAME.put("242","UnderlyingIssueDate");
    MAP_FIELD_CODE_TO_NAME.put("243","UnderlyingRepoCollateralSecurityType");
    MAP_FIELD_CODE_TO_NAME.put("244","UnderlyingRepurchaseTerm");
    MAP_FIELD_CODE_TO_NAME.put("245","UnderlyingRepurchaseRate");
    MAP_FIELD_CODE_TO_NAME.put("246","UnderlyingFactor");
    MAP_FIELD_CODE_TO_NAME.put("247","UnderlyingRedemptionDate");
    MAP_FIELD_CODE_TO_NAME.put("248","LegCouponPaymentDate");
    MAP_FIELD_CODE_TO_NAME.put("249","LegIssueDate");
    MAP_FIELD_CODE_TO_NAME.put("250","LegRepoCollateralSecurityType");
    MAP_FIELD_CODE_TO_NAME.put("251","LegRepurchaseTerm");
    MAP_FIELD_CODE_TO_NAME.put("252","LegRepurchaseRate");
    MAP_FIELD_CODE_TO_NAME.put("253","LegFactor");
    MAP_FIELD_CODE_TO_NAME.put("254","LegRedemptionDate");
    MAP_FIELD_CODE_TO_NAME.put("255","CreditRating");
    MAP_FIELD_CODE_TO_NAME.put("256","UnderlyingCreditRating");
    MAP_FIELD_CODE_TO_NAME.put("257","LegCreditRating");
    MAP_FIELD_CODE_TO_NAME.put("258","TradedFlatSwitch");
    MAP_FIELD_CODE_TO_NAME.put("259","BasisFeatureDate");
    MAP_FIELD_CODE_TO_NAME.put("260","BasisFeaturePrice");
    MAP_FIELD_CODE_TO_NAME.put("262","MDReqID");
    MAP_FIELD_CODE_TO_NAME.put("263","SubscriptionRequestType");
    MAP_FIELD_CODE_TO_NAME.put("264","MarketDepth");
    MAP_FIELD_CODE_TO_NAME.put("265","MDUpdateType");
    MAP_FIELD_CODE_TO_NAME.put("266","AggregatedBook");
    MAP_FIELD_CODE_TO_NAME.put("267","NoMDEntryTypes");
    MAP_FIELD_CODE_TO_NAME.put("268","NoMDEntries");
    MAP_FIELD_CODE_TO_NAME.put("269","MDEntryType");
    MAP_FIELD_CODE_TO_NAME.put("270","MDEntryPx");
    MAP_FIELD_CODE_TO_NAME.put("271","MDEntrySize");
    MAP_FIELD_CODE_TO_NAME.put("272","MDEntryDate");
    MAP_FIELD_CODE_TO_NAME.put("273","MDEntryTime");
    MAP_FIELD_CODE_TO_NAME.put("274","TickDirection");
    MAP_FIELD_CODE_TO_NAME.put("275","MDMkt");
    MAP_FIELD_CODE_TO_NAME.put("276","QuoteCondition");
    MAP_FIELD_CODE_TO_NAME.put("277","TradeCondition");
    MAP_FIELD_CODE_TO_NAME.put("278","MDEntryID");
    MAP_FIELD_CODE_TO_NAME.put("279","MDUpdateAction");
    MAP_FIELD_CODE_TO_NAME.put("280","MDEntryRefID");
    MAP_FIELD_CODE_TO_NAME.put("281","MDReqRejReason");
    MAP_FIELD_CODE_TO_NAME.put("282","MDEntryOriginator");
    MAP_FIELD_CODE_TO_NAME.put("283","LocationID");
    MAP_FIELD_CODE_TO_NAME.put("284","DeskID");
    MAP_FIELD_CODE_TO_NAME.put("285","DeleteReason");
    MAP_FIELD_CODE_TO_NAME.put("286","OpenCloseSettlFlag");
    MAP_FIELD_CODE_TO_NAME.put("287","SellerDays");
    MAP_FIELD_CODE_TO_NAME.put("288","MDEntryBuyer");
    MAP_FIELD_CODE_TO_NAME.put("289","MDEntrySeller");
    MAP_FIELD_CODE_TO_NAME.put("290","MDEntryPositionNo");
    MAP_FIELD_CODE_TO_NAME.put("291","FinancialStatus");
    MAP_FIELD_CODE_TO_NAME.put("292","CorporateAction");
    MAP_FIELD_CODE_TO_NAME.put("293","DefBidSize");
    MAP_FIELD_CODE_TO_NAME.put("294","DefOfferSize");
    MAP_FIELD_CODE_TO_NAME.put("295","NoQuoteEntries");
    MAP_FIELD_CODE_TO_NAME.put("296","NoQuoteSets");
    MAP_FIELD_CODE_TO_NAME.put("297","QuoteStatus");
    MAP_FIELD_CODE_TO_NAME.put("298","QuoteCancelType");
    MAP_FIELD_CODE_TO_NAME.put("299","QuoteEntryID");
    MAP_FIELD_CODE_TO_NAME.put("300","QuoteRejectReason");
    MAP_FIELD_CODE_TO_NAME.put("301","QuoteResponseLevel");
    MAP_FIELD_CODE_TO_NAME.put("302","QuoteSetID");
    MAP_FIELD_CODE_TO_NAME.put("303","QuoteRequestType");
    MAP_FIELD_CODE_TO_NAME.put("304","TotNoQuoteEntries");
    MAP_FIELD_CODE_TO_NAME.put("305","UnderlyingSecurityIDSource");
    MAP_FIELD_CODE_TO_NAME.put("306","UnderlyingIssuer");
    MAP_FIELD_CODE_TO_NAME.put("307","UnderlyingSecurityDesc");
    MAP_FIELD_CODE_TO_NAME.put("308","UnderlyingSecurityExchange");
    MAP_FIELD_CODE_TO_NAME.put("309","UnderlyingSecurityID");
    MAP_FIELD_CODE_TO_NAME.put("310","UnderlyingSecurityType");
    MAP_FIELD_CODE_TO_NAME.put("311","UnderlyingSymbol");
    MAP_FIELD_CODE_TO_NAME.put("312","UnderlyingSymbolSfx");
    MAP_FIELD_CODE_TO_NAME.put("313","UnderlyingMaturityMonthYear");
    MAP_FIELD_CODE_TO_NAME.put("314","UnderlyingMaturityDay");
    MAP_FIELD_CODE_TO_NAME.put("315","UnderlyingPutOrCall");
    MAP_FIELD_CODE_TO_NAME.put("316","UnderlyingStrikePrice");
    MAP_FIELD_CODE_TO_NAME.put("317","UnderlyingOptAttribute");
    MAP_FIELD_CODE_TO_NAME.put("318","UnderlyingCurrency");
    MAP_FIELD_CODE_TO_NAME.put("319","RatioQty");
    MAP_FIELD_CODE_TO_NAME.put("320","SecurityReqID");
    MAP_FIELD_CODE_TO_NAME.put("321","SecurityRequestType");
    MAP_FIELD_CODE_TO_NAME.put("322","SecurityResponseID");
    MAP_FIELD_CODE_TO_NAME.put("323","SecurityResponseType");
    MAP_FIELD_CODE_TO_NAME.put("324","SecurityStatusReqID");
    MAP_FIELD_CODE_TO_NAME.put("325","UnsolicitedIndicator");
    MAP_FIELD_CODE_TO_NAME.put("326","SecurityTradingStatus");
    MAP_FIELD_CODE_TO_NAME.put("327","HaltReason");
    MAP_FIELD_CODE_TO_NAME.put("328","InViewOfCommon");
    MAP_FIELD_CODE_TO_NAME.put("329","DueToRelated");
    MAP_FIELD_CODE_TO_NAME.put("330","BuyVolume");
    MAP_FIELD_CODE_TO_NAME.put("331","SellVolume");
    MAP_FIELD_CODE_TO_NAME.put("332","HighPx");
    MAP_FIELD_CODE_TO_NAME.put("333","LowPx");
    MAP_FIELD_CODE_TO_NAME.put("334","Adjustment");
    MAP_FIELD_CODE_TO_NAME.put("335","TradSesReqID");
    MAP_FIELD_CODE_TO_NAME.put("336","TradingSessionID");
    MAP_FIELD_CODE_TO_NAME.put("337","ContraTrader");
    MAP_FIELD_CODE_TO_NAME.put("338","TradSesMethod");
    MAP_FIELD_CODE_TO_NAME.put("339","TradSesMode");
    MAP_FIELD_CODE_TO_NAME.put("340","TradSesStatus");
    MAP_FIELD_CODE_TO_NAME.put("341","TradSesStartTime");
    MAP_FIELD_CODE_TO_NAME.put("342","TradSesOpenTime");
    MAP_FIELD_CODE_TO_NAME.put("343","TradSesPreCloseTime");
    MAP_FIELD_CODE_TO_NAME.put("344","TradSesCloseTime");
    MAP_FIELD_CODE_TO_NAME.put("345","TradSesEndTime");
    MAP_FIELD_CODE_TO_NAME.put("346","NumberOfOrders");
    MAP_FIELD_CODE_TO_NAME.put("347","MessageEncoding");
    MAP_FIELD_CODE_TO_NAME.put("348","EncodedIssuerLen");
    MAP_FIELD_CODE_TO_NAME.put("349","EncodedIssuer");
    MAP_FIELD_CODE_TO_NAME.put("350","EncodedSecurityDescLen");
    MAP_FIELD_CODE_TO_NAME.put("351","EncodedSecurityDesc");
    MAP_FIELD_CODE_TO_NAME.put("352","EncodedListExecInstLen");
    MAP_FIELD_CODE_TO_NAME.put("353","EncodedListExecInst");
    MAP_FIELD_CODE_TO_NAME.put("354","EncodedTextLen");
    MAP_FIELD_CODE_TO_NAME.put("355","EncodedText");
    MAP_FIELD_CODE_TO_NAME.put("356","EncodedSubjectLen");
    MAP_FIELD_CODE_TO_NAME.put("357","EncodedSubject");
    MAP_FIELD_CODE_TO_NAME.put("358","EncodedHeadlineLen");
    MAP_FIELD_CODE_TO_NAME.put("359","EncodedHeadline");
    MAP_FIELD_CODE_TO_NAME.put("360","EncodedAllocTextLen");
    MAP_FIELD_CODE_TO_NAME.put("361","EncodedAllocText");
    MAP_FIELD_CODE_TO_NAME.put("362","EncodedUnderlyingIssuerLen");
    MAP_FIELD_CODE_TO_NAME.put("363","EncodedUnderlyingIssuer");
    MAP_FIELD_CODE_TO_NAME.put("364","EncodedUnderlyingSecurityDescLen");
    MAP_FIELD_CODE_TO_NAME.put("365","EncodedUnderlyingSecurityDesc");
    MAP_FIELD_CODE_TO_NAME.put("366","AllocPrice");
    MAP_FIELD_CODE_TO_NAME.put("367","QuoteSetValidUntilTime");
    MAP_FIELD_CODE_TO_NAME.put("368","QuoteEntryRejectReason");
    MAP_FIELD_CODE_TO_NAME.put("369","LastMsgSeqNumProcessed");
    MAP_FIELD_CODE_TO_NAME.put("370","OnBehalfOfSendingTime");
    MAP_FIELD_CODE_TO_NAME.put("371","RefTagID");
    MAP_FIELD_CODE_TO_NAME.put("372","RefMsgType");
    MAP_FIELD_CODE_TO_NAME.put("373","SessionRejectReason");
    MAP_FIELD_CODE_TO_NAME.put("374","BidRequestTransType");
    MAP_FIELD_CODE_TO_NAME.put("375","ContraBroker");
    MAP_FIELD_CODE_TO_NAME.put("376","ComplianceID");
    MAP_FIELD_CODE_TO_NAME.put("377","SolicitedFlag");
    MAP_FIELD_CODE_TO_NAME.put("378","ExecRestatementReason");
    MAP_FIELD_CODE_TO_NAME.put("379","BusinessRejectRefID");
    MAP_FIELD_CODE_TO_NAME.put("380","BusinessRejectReason");
    MAP_FIELD_CODE_TO_NAME.put("381","GrossTradeAmt");
    MAP_FIELD_CODE_TO_NAME.put("382","NoContraBrokers");
    MAP_FIELD_CODE_TO_NAME.put("383","MaxMessageSize");
    MAP_FIELD_CODE_TO_NAME.put("384","NoMsgTypes");
    MAP_FIELD_CODE_TO_NAME.put("385","MsgDirection");
    MAP_FIELD_CODE_TO_NAME.put("386","NoTradingSessions");
    MAP_FIELD_CODE_TO_NAME.put("387","TotalVolumeTraded");
    MAP_FIELD_CODE_TO_NAME.put("388","DiscretionInst");
    MAP_FIELD_CODE_TO_NAME.put("389","DiscretionOffsetValue");
    MAP_FIELD_CODE_TO_NAME.put("390","BidID");
    MAP_FIELD_CODE_TO_NAME.put("391","ClientBidID");
    MAP_FIELD_CODE_TO_NAME.put("392","ListName");
    MAP_FIELD_CODE_TO_NAME.put("393","TotNoRelatedSym");
    MAP_FIELD_CODE_TO_NAME.put("394","BidType");
    MAP_FIELD_CODE_TO_NAME.put("395","NumTickets");
    MAP_FIELD_CODE_TO_NAME.put("396","SideValue1");
    MAP_FIELD_CODE_TO_NAME.put("397","SideValue2");
    MAP_FIELD_CODE_TO_NAME.put("398","NoBidDescriptors");
    MAP_FIELD_CODE_TO_NAME.put("399","BidDescriptorType");
    MAP_FIELD_CODE_TO_NAME.put("400","BidDescriptor");
    MAP_FIELD_CODE_TO_NAME.put("401","SideValueInd");
    MAP_FIELD_CODE_TO_NAME.put("402","LiquidityPctLow");
    MAP_FIELD_CODE_TO_NAME.put("403","LiquidityPctHigh");
    MAP_FIELD_CODE_TO_NAME.put("404","LiquidityValue");
    MAP_FIELD_CODE_TO_NAME.put("405","EFPTrackingError");
    MAP_FIELD_CODE_TO_NAME.put("406","FairValue");
    MAP_FIELD_CODE_TO_NAME.put("407","OutsideIndexPct");
    MAP_FIELD_CODE_TO_NAME.put("408","ValueOfFutures");
    MAP_FIELD_CODE_TO_NAME.put("409","LiquidityIndType");
    MAP_FIELD_CODE_TO_NAME.put("410","WtAverageLiquidity");
    MAP_FIELD_CODE_TO_NAME.put("411","ExchangeForPhysical");
    MAP_FIELD_CODE_TO_NAME.put("412","OutMainCntryUIndex");
    MAP_FIELD_CODE_TO_NAME.put("413","CrossPercent");
    MAP_FIELD_CODE_TO_NAME.put("414","ProgRptReqs");
    MAP_FIELD_CODE_TO_NAME.put("415","ProgPeriodInterval");
    MAP_FIELD_CODE_TO_NAME.put("416","IncTaxInd");
    MAP_FIELD_CODE_TO_NAME.put("417","NumBidders");
    MAP_FIELD_CODE_TO_NAME.put("418","BidTradeType");
    MAP_FIELD_CODE_TO_NAME.put("419","BasisPxType");
    MAP_FIELD_CODE_TO_NAME.put("420","NoBidComponents");
    MAP_FIELD_CODE_TO_NAME.put("421","Country");
    MAP_FIELD_CODE_TO_NAME.put("422","TotNoStrikes");
    MAP_FIELD_CODE_TO_NAME.put("423","PriceType");
    MAP_FIELD_CODE_TO_NAME.put("424","DayOrderQty");
    MAP_FIELD_CODE_TO_NAME.put("425","DayCumQty");
    MAP_FIELD_CODE_TO_NAME.put("426","DayAvgPx");
    MAP_FIELD_CODE_TO_NAME.put("427","GTBookingInst");
    MAP_FIELD_CODE_TO_NAME.put("428","NoStrikes");
    MAP_FIELD_CODE_TO_NAME.put("429","ListStatusType");
    MAP_FIELD_CODE_TO_NAME.put("430","NetGrossInd");
    MAP_FIELD_CODE_TO_NAME.put("431","ListOrderStatus");
    MAP_FIELD_CODE_TO_NAME.put("432","ExpireDate");
    MAP_FIELD_CODE_TO_NAME.put("433","ListExecInstType");
    MAP_FIELD_CODE_TO_NAME.put("434","CxlRejResponseTo");
    MAP_FIELD_CODE_TO_NAME.put("435","UnderlyingCouponRate");
    MAP_FIELD_CODE_TO_NAME.put("436","UnderlyingContractMultiplier");
    MAP_FIELD_CODE_TO_NAME.put("437","ContraTradeQty");
    MAP_FIELD_CODE_TO_NAME.put("438","ContraTradeTime");
    MAP_FIELD_CODE_TO_NAME.put("439","ClearingFirm");
    MAP_FIELD_CODE_TO_NAME.put("440","ClearingAccount");
    MAP_FIELD_CODE_TO_NAME.put("441","LiquidityNumSecurities");
    MAP_FIELD_CODE_TO_NAME.put("442","MultiLegReportingType");
    MAP_FIELD_CODE_TO_NAME.put("443","StrikeTime");
    MAP_FIELD_CODE_TO_NAME.put("444","ListStatusText");
    MAP_FIELD_CODE_TO_NAME.put("445","EncodedListStatusTextLen");
    MAP_FIELD_CODE_TO_NAME.put("446","EncodedListStatusText");
    MAP_FIELD_CODE_TO_NAME.put("447","PartyIDSource");
    MAP_FIELD_CODE_TO_NAME.put("448","PartyID");
    MAP_FIELD_CODE_TO_NAME.put("449","TotalVolumeTradedDate");
    MAP_FIELD_CODE_TO_NAME.put("450","TotalVolumeTraded Time");
    MAP_FIELD_CODE_TO_NAME.put("451","NetChgPrevDay");
    MAP_FIELD_CODE_TO_NAME.put("452","PartyRole");
    MAP_FIELD_CODE_TO_NAME.put("453","NoPartyIDs");
    MAP_FIELD_CODE_TO_NAME.put("454","NoSecurityAltID");
    MAP_FIELD_CODE_TO_NAME.put("455","SecurityAltID");
    MAP_FIELD_CODE_TO_NAME.put("456","SecurityAltIDSource");
    MAP_FIELD_CODE_TO_NAME.put("457","NoUnderlyingSecurityAltID");
    MAP_FIELD_CODE_TO_NAME.put("458","UnderlyingSecurityAltID");
    MAP_FIELD_CODE_TO_NAME.put("459","UnderlyingSecurityAltIDSource");
    MAP_FIELD_CODE_TO_NAME.put("460","Product");
    MAP_FIELD_CODE_TO_NAME.put("461","CFICode");
    MAP_FIELD_CODE_TO_NAME.put("462","UnderlyingProduct");
    MAP_FIELD_CODE_TO_NAME.put("463","UnderlyingCFICode");
    MAP_FIELD_CODE_TO_NAME.put("464","TestMessageIndicator");
    MAP_FIELD_CODE_TO_NAME.put("465","QuantityType");
    MAP_FIELD_CODE_TO_NAME.put("466","BookingRefID");
    MAP_FIELD_CODE_TO_NAME.put("467","IndividualAllocID");
    MAP_FIELD_CODE_TO_NAME.put("468","RoundingDirection");
    MAP_FIELD_CODE_TO_NAME.put("469","RoundingModulus");
    MAP_FIELD_CODE_TO_NAME.put("470","CountryOfIssue");
    MAP_FIELD_CODE_TO_NAME.put("471","StateOrProvinceOfIssue");
    MAP_FIELD_CODE_TO_NAME.put("472","LocaleOfIssue");
    MAP_FIELD_CODE_TO_NAME.put("473","NoRegistDtls");
    MAP_FIELD_CODE_TO_NAME.put("474","MailingDtls");
    MAP_FIELD_CODE_TO_NAME.put("475","InvestorCountryOfResidence");
    MAP_FIELD_CODE_TO_NAME.put("476","PaymentRef");
    MAP_FIELD_CODE_TO_NAME.put("477","DistribPaymentMethod");
    MAP_FIELD_CODE_TO_NAME.put("478","CashDistribCurr");
    MAP_FIELD_CODE_TO_NAME.put("479","CommCurrency");
    MAP_FIELD_CODE_TO_NAME.put("480","CancellationRights");
    MAP_FIELD_CODE_TO_NAME.put("481","MoneyLaunderingStatus");
    MAP_FIELD_CODE_TO_NAME.put("482","MailingInst");
    MAP_FIELD_CODE_TO_NAME.put("483","TransBkdTime");
    MAP_FIELD_CODE_TO_NAME.put("484","ExecPriceType");
    MAP_FIELD_CODE_TO_NAME.put("485","ExecPriceAdjustment");
    MAP_FIELD_CODE_TO_NAME.put("486","DateOfBirth");
    MAP_FIELD_CODE_TO_NAME.put("487","TradeReportTransType");
    MAP_FIELD_CODE_TO_NAME.put("488","CardHolderName");
    MAP_FIELD_CODE_TO_NAME.put("489","CardNumber");
    MAP_FIELD_CODE_TO_NAME.put("490","CardExpDate");
    MAP_FIELD_CODE_TO_NAME.put("491","CardIssNum");
    MAP_FIELD_CODE_TO_NAME.put("492","PaymentMethod");
    MAP_FIELD_CODE_TO_NAME.put("493","RegistAcctType");
    MAP_FIELD_CODE_TO_NAME.put("494","Designation");
    MAP_FIELD_CODE_TO_NAME.put("495","TaxAdvantageType");
    MAP_FIELD_CODE_TO_NAME.put("496","RegistRejReasonText");
    MAP_FIELD_CODE_TO_NAME.put("497","FundRenewWaiv");
    MAP_FIELD_CODE_TO_NAME.put("498","CashDistribAgentName");
    MAP_FIELD_CODE_TO_NAME.put("499","CashDistribAgentCode");
    MAP_FIELD_CODE_TO_NAME.put("500","CashDistribAgentAcctNumber");
    MAP_FIELD_CODE_TO_NAME.put("501","CashDistribPayRef");
    MAP_FIELD_CODE_TO_NAME.put("502","CashDistribAgentAcctName");
    MAP_FIELD_CODE_TO_NAME.put("503","CardStartDate");
    MAP_FIELD_CODE_TO_NAME.put("504","PaymentDate");
    MAP_FIELD_CODE_TO_NAME.put("505","PaymentRemitterID");
    MAP_FIELD_CODE_TO_NAME.put("506","RegistStatus");
    MAP_FIELD_CODE_TO_NAME.put("507","RegistRejReasonCode");
    MAP_FIELD_CODE_TO_NAME.put("508","RegistRefID");
    MAP_FIELD_CODE_TO_NAME.put("509","RegistDtls");
    MAP_FIELD_CODE_TO_NAME.put("510","NoDistribInsts");
    MAP_FIELD_CODE_TO_NAME.put("511","RegistEmail");
    MAP_FIELD_CODE_TO_NAME.put("512","DistribPercentage");
    MAP_FIELD_CODE_TO_NAME.put("513","RegistID");
    MAP_FIELD_CODE_TO_NAME.put("514","RegistTransType");
    MAP_FIELD_CODE_TO_NAME.put("515","ExecValuationPoint");
    MAP_FIELD_CODE_TO_NAME.put("516","OrderPercent");
    MAP_FIELD_CODE_TO_NAME.put("517","OwnershipType");
    MAP_FIELD_CODE_TO_NAME.put("518","NoContAmts");
    MAP_FIELD_CODE_TO_NAME.put("519","ContAmtType");
    MAP_FIELD_CODE_TO_NAME.put("520","ContAmtValue");
    MAP_FIELD_CODE_TO_NAME.put("521","ContAmtCurr");
    MAP_FIELD_CODE_TO_NAME.put("522","OwnerType");
    MAP_FIELD_CODE_TO_NAME.put("523","PartySubID");
    MAP_FIELD_CODE_TO_NAME.put("524","NestedPartyID");
    MAP_FIELD_CODE_TO_NAME.put("525","NestedPartyIDSource");
    MAP_FIELD_CODE_TO_NAME.put("526","SecondaryClOrdID");
    MAP_FIELD_CODE_TO_NAME.put("527","SecondaryExecID");
    MAP_FIELD_CODE_TO_NAME.put("528","OrderCapacity");
    MAP_FIELD_CODE_TO_NAME.put("529","OrderRestrictions");
    MAP_FIELD_CODE_TO_NAME.put("530","MassCancelRequestType");
    MAP_FIELD_CODE_TO_NAME.put("531","MassCancelResponse");
    MAP_FIELD_CODE_TO_NAME.put("532","MassCancelRejectReason");
    MAP_FIELD_CODE_TO_NAME.put("533","TotalAffectedOrders");
    MAP_FIELD_CODE_TO_NAME.put("534","NoAffectedOrders");
    MAP_FIELD_CODE_TO_NAME.put("535","AffectedOrderID");
    MAP_FIELD_CODE_TO_NAME.put("536","AffectedSecondaryOrderID");
    MAP_FIELD_CODE_TO_NAME.put("537","QuoteType");
    MAP_FIELD_CODE_TO_NAME.put("538","NestedPartyRole");
    MAP_FIELD_CODE_TO_NAME.put("539","NoNestedPartyIDs");
    MAP_FIELD_CODE_TO_NAME.put("540","TotalAccruedInterestAmt");
    MAP_FIELD_CODE_TO_NAME.put("541","MaturityDate");
    MAP_FIELD_CODE_TO_NAME.put("542","UnderlyingMaturityDate");
    MAP_FIELD_CODE_TO_NAME.put("543","InstrRegistry");
    MAP_FIELD_CODE_TO_NAME.put("544","CashMargin");
    MAP_FIELD_CODE_TO_NAME.put("545","NestedPartySubID");
    MAP_FIELD_CODE_TO_NAME.put("546","Scope");
    MAP_FIELD_CODE_TO_NAME.put("547","MDImplicitDelete");
    MAP_FIELD_CODE_TO_NAME.put("548","CrossID");
    MAP_FIELD_CODE_TO_NAME.put("549","CrossType");
    MAP_FIELD_CODE_TO_NAME.put("550","CrossPrioritization");
    MAP_FIELD_CODE_TO_NAME.put("551","OrigCrossID");
    MAP_FIELD_CODE_TO_NAME.put("552","NoSides");
    MAP_FIELD_CODE_TO_NAME.put("553","Username");
    MAP_FIELD_CODE_TO_NAME.put("554","Password");
    MAP_FIELD_CODE_TO_NAME.put("555","NoLegs");
    MAP_FIELD_CODE_TO_NAME.put("556","LegCurrency");
    MAP_FIELD_CODE_TO_NAME.put("557","TotNoSecurityTypes");
    MAP_FIELD_CODE_TO_NAME.put("558","NoSecurityTypes");
    MAP_FIELD_CODE_TO_NAME.put("559","SecurityListRequestType");
    MAP_FIELD_CODE_TO_NAME.put("560","SecurityRequestResult");
    MAP_FIELD_CODE_TO_NAME.put("561","RoundLot");
    MAP_FIELD_CODE_TO_NAME.put("562","MinTradeVol");
    MAP_FIELD_CODE_TO_NAME.put("563","MultiLegRptTypeReq");
    MAP_FIELD_CODE_TO_NAME.put("564","LegPositionEffect");
    MAP_FIELD_CODE_TO_NAME.put("565","LegCoveredOrUncovered");
    MAP_FIELD_CODE_TO_NAME.put("566","LegPrice");
    MAP_FIELD_CODE_TO_NAME.put("567","TradSesStatusRejReason");
    MAP_FIELD_CODE_TO_NAME.put("568","TradeRequestID");
    MAP_FIELD_CODE_TO_NAME.put("569","TradeRequestType");
    MAP_FIELD_CODE_TO_NAME.put("570","PreviouslyReported");
    MAP_FIELD_CODE_TO_NAME.put("571","TradeReportID");
    MAP_FIELD_CODE_TO_NAME.put("572","TradeReportRefID");
    MAP_FIELD_CODE_TO_NAME.put("573","MatchStatus");
    MAP_FIELD_CODE_TO_NAME.put("574","MatchType");
    MAP_FIELD_CODE_TO_NAME.put("575","OddLot");
    MAP_FIELD_CODE_TO_NAME.put("576","NoClearingInstructions");
    MAP_FIELD_CODE_TO_NAME.put("577","ClearingInstruction");
    MAP_FIELD_CODE_TO_NAME.put("578","TradeInputSource");
    MAP_FIELD_CODE_TO_NAME.put("579","TradeInputDevice");
    MAP_FIELD_CODE_TO_NAME.put("580","NoDates");
    MAP_FIELD_CODE_TO_NAME.put("581","AccountType");
    MAP_FIELD_CODE_TO_NAME.put("582","CustOrderCapacity");
    MAP_FIELD_CODE_TO_NAME.put("583","ClOrdLinkID");
    MAP_FIELD_CODE_TO_NAME.put("584","MassStatusReqID");
    MAP_FIELD_CODE_TO_NAME.put("585","MassStatusReqType");
    MAP_FIELD_CODE_TO_NAME.put("586","OrigOrdModTime");
    MAP_FIELD_CODE_TO_NAME.put("587","LegSettlType");
    MAP_FIELD_CODE_TO_NAME.put("588","LegSettlDate");
    MAP_FIELD_CODE_TO_NAME.put("589","DayBookingInst");
    MAP_FIELD_CODE_TO_NAME.put("590","BookingUnit");
    MAP_FIELD_CODE_TO_NAME.put("591","PreallocMethod");
    MAP_FIELD_CODE_TO_NAME.put("592","UnderlyingCountryOfIssue");
    MAP_FIELD_CODE_TO_NAME.put("593","UnderlyingStateOrProvinceOfIssue");
    MAP_FIELD_CODE_TO_NAME.put("594","UnderlyingLocaleOfIssue");
    MAP_FIELD_CODE_TO_NAME.put("595","UnderlyingInstrRegistry");
    MAP_FIELD_CODE_TO_NAME.put("596","LegCountryOfIssue");
    MAP_FIELD_CODE_TO_NAME.put("597","LegStateOrProvinceOfIssue");
    MAP_FIELD_CODE_TO_NAME.put("598","LegLocaleOfIssue");
    MAP_FIELD_CODE_TO_NAME.put("599","LegInstrRegistry");
    MAP_FIELD_CODE_TO_NAME.put("600","LegSymbol");
    MAP_FIELD_CODE_TO_NAME.put("601","LegSymbolSfx");
    MAP_FIELD_CODE_TO_NAME.put("602","LegSecurityID");
    MAP_FIELD_CODE_TO_NAME.put("603","LegSecurityIDSource");
    MAP_FIELD_CODE_TO_NAME.put("604","NoLegSecurityAltID");
    MAP_FIELD_CODE_TO_NAME.put("605","LegSecurityAltID");
    MAP_FIELD_CODE_TO_NAME.put("606","LegSecurityAltIDSource");
    MAP_FIELD_CODE_TO_NAME.put("607","LegProduct");
    MAP_FIELD_CODE_TO_NAME.put("608","LegCFICode");
    MAP_FIELD_CODE_TO_NAME.put("609","LegSecurityType");
    MAP_FIELD_CODE_TO_NAME.put("610","LegMaturityMonthYear");
    MAP_FIELD_CODE_TO_NAME.put("611","LegMaturityDate");
    MAP_FIELD_CODE_TO_NAME.put("612","LegStrikePrice");
    MAP_FIELD_CODE_TO_NAME.put("613","LegOptAttribute");
    MAP_FIELD_CODE_TO_NAME.put("614","LegContractMultiplier");
    MAP_FIELD_CODE_TO_NAME.put("615","LegCouponRate");
    MAP_FIELD_CODE_TO_NAME.put("616","LegSecurityExchange");
    MAP_FIELD_CODE_TO_NAME.put("617","LegIssuer");
    MAP_FIELD_CODE_TO_NAME.put("618","EncodedLegIssuerLen");
    MAP_FIELD_CODE_TO_NAME.put("619","EncodedLegIssuer");
    MAP_FIELD_CODE_TO_NAME.put("620","LegSecurityDesc");
    MAP_FIELD_CODE_TO_NAME.put("621","EncodedLegSecurityDescLen");
    MAP_FIELD_CODE_TO_NAME.put("622","EncodedLegSecurityDesc");
    MAP_FIELD_CODE_TO_NAME.put("623","LegRatioQty");
    MAP_FIELD_CODE_TO_NAME.put("624","LegSide");
    MAP_FIELD_CODE_TO_NAME.put("625","TradingSessionSubID");
    MAP_FIELD_CODE_TO_NAME.put("626","AllocType");
    MAP_FIELD_CODE_TO_NAME.put("627","NoHops");
    MAP_FIELD_CODE_TO_NAME.put("628","HopCompID");
    MAP_FIELD_CODE_TO_NAME.put("629","HopSendingTime");
    MAP_FIELD_CODE_TO_NAME.put("630","HopRefID");
    MAP_FIELD_CODE_TO_NAME.put("631","MidPx");
    MAP_FIELD_CODE_TO_NAME.put("632","BidYield");
    MAP_FIELD_CODE_TO_NAME.put("633","MidYield");
    MAP_FIELD_CODE_TO_NAME.put("634","OfferYield");
    MAP_FIELD_CODE_TO_NAME.put("635","ClearingFeeIndicator");
    MAP_FIELD_CODE_TO_NAME.put("636","WorkingIndicator");
    MAP_FIELD_CODE_TO_NAME.put("637","LegLastPx");
    MAP_FIELD_CODE_TO_NAME.put("638","PriorityIndicator");
    MAP_FIELD_CODE_TO_NAME.put("639","PriceImprovement");
    MAP_FIELD_CODE_TO_NAME.put("640","Price2");
    MAP_FIELD_CODE_TO_NAME.put("641","LastForwardPoints2");
    MAP_FIELD_CODE_TO_NAME.put("642","BidForwardPoints2");
    MAP_FIELD_CODE_TO_NAME.put("643","OfferForwardPoints2");
    MAP_FIELD_CODE_TO_NAME.put("644","RFQReqID");
    MAP_FIELD_CODE_TO_NAME.put("645","MktBidPx");
    MAP_FIELD_CODE_TO_NAME.put("646","MktOfferPx");
    MAP_FIELD_CODE_TO_NAME.put("647","MinBidSize");
    MAP_FIELD_CODE_TO_NAME.put("648","MinOfferSize");
    MAP_FIELD_CODE_TO_NAME.put("649","QuoteStatusReqID");
    MAP_FIELD_CODE_TO_NAME.put("650","LegalConfirm");
    MAP_FIELD_CODE_TO_NAME.put("651","UnderlyingLastPx");
    MAP_FIELD_CODE_TO_NAME.put("652","UnderlyingLastQty");
    MAP_FIELD_CODE_TO_NAME.put("653","SecDefStatus");
    MAP_FIELD_CODE_TO_NAME.put("654","LegRefID");
    MAP_FIELD_CODE_TO_NAME.put("655","ContraLegRefID");
    MAP_FIELD_CODE_TO_NAME.put("656","SettlCurrBidFxRate");
    MAP_FIELD_CODE_TO_NAME.put("657","SettlCurrOfferFxRate");
    MAP_FIELD_CODE_TO_NAME.put("658","QuoteRequestRejectReason");
    MAP_FIELD_CODE_TO_NAME.put("659","SideComplianceID");
    MAP_FIELD_CODE_TO_NAME.put("660","AcctIDSource");
    MAP_FIELD_CODE_TO_NAME.put("661","AllocAcctIDSource");
    MAP_FIELD_CODE_TO_NAME.put("662","BenchmarkPrice");
    MAP_FIELD_CODE_TO_NAME.put("663","BenchmarkPriceType");
    MAP_FIELD_CODE_TO_NAME.put("664","ConfirmID");
    MAP_FIELD_CODE_TO_NAME.put("665","ConfirmStatus");
    MAP_FIELD_CODE_TO_NAME.put("666","ConfirmTransType");
    MAP_FIELD_CODE_TO_NAME.put("667","ContractSettlMonth");
    MAP_FIELD_CODE_TO_NAME.put("668","DeliveryForm");
    MAP_FIELD_CODE_TO_NAME.put("669","LastParPx");
    MAP_FIELD_CODE_TO_NAME.put("670","NoLegAllocs");
    MAP_FIELD_CODE_TO_NAME.put("671","LegAllocAccount");
    MAP_FIELD_CODE_TO_NAME.put("672","LegIndividualAllocID");
    MAP_FIELD_CODE_TO_NAME.put("673","LegAllocQty");
    MAP_FIELD_CODE_TO_NAME.put("674","LegAllocAcctIDSource");
    MAP_FIELD_CODE_TO_NAME.put("675","LegSettlCurrency");
    MAP_FIELD_CODE_TO_NAME.put("676","LegBenchmarkCurveCurrency");
    MAP_FIELD_CODE_TO_NAME.put("677","LegBenchmarkCurveName");
    MAP_FIELD_CODE_TO_NAME.put("678","LegBenchmarkCurvePoint");
    MAP_FIELD_CODE_TO_NAME.put("679","LegBenchmarkPrice");
    MAP_FIELD_CODE_TO_NAME.put("680","LegBenchmarkPriceType");
    MAP_FIELD_CODE_TO_NAME.put("681","LegBidPx");
    MAP_FIELD_CODE_TO_NAME.put("682","LegIOIQty");
    MAP_FIELD_CODE_TO_NAME.put("683","NoLegStipulations");
    MAP_FIELD_CODE_TO_NAME.put("684","LegOfferPx");
    MAP_FIELD_CODE_TO_NAME.put("685","LegOrderQty");
    MAP_FIELD_CODE_TO_NAME.put("686","LegPriceType");
    MAP_FIELD_CODE_TO_NAME.put("687","LegQty");
    MAP_FIELD_CODE_TO_NAME.put("688","LegStipulationType");
    MAP_FIELD_CODE_TO_NAME.put("689","LegStipulationValue");
    MAP_FIELD_CODE_TO_NAME.put("690","LegSwapType");
    MAP_FIELD_CODE_TO_NAME.put("691","Pool");
    MAP_FIELD_CODE_TO_NAME.put("692","QuotePriceType");
    MAP_FIELD_CODE_TO_NAME.put("693","QuoteRespID");
    MAP_FIELD_CODE_TO_NAME.put("694","QuoteRespType");
    MAP_FIELD_CODE_TO_NAME.put("695","QuoteQualifier");
    MAP_FIELD_CODE_TO_NAME.put("696","YieldRedemptionDate");
    MAP_FIELD_CODE_TO_NAME.put("697","YieldRedemptionPrice");
    MAP_FIELD_CODE_TO_NAME.put("698","YieldRedemptionPriceType");
    MAP_FIELD_CODE_TO_NAME.put("699","BenchmarkSecurityID");
    MAP_FIELD_CODE_TO_NAME.put("700","ReversalIndicator");
    MAP_FIELD_CODE_TO_NAME.put("701","YieldCalcDate");
    MAP_FIELD_CODE_TO_NAME.put("702","NoPositions");
    MAP_FIELD_CODE_TO_NAME.put("703","PosType");
    MAP_FIELD_CODE_TO_NAME.put("704","LongQty");
    MAP_FIELD_CODE_TO_NAME.put("705","ShortQty");
    MAP_FIELD_CODE_TO_NAME.put("706","PosQtyStatus");
    MAP_FIELD_CODE_TO_NAME.put("707","PosAmtType");
    MAP_FIELD_CODE_TO_NAME.put("708","PosAmt");
    MAP_FIELD_CODE_TO_NAME.put("709","PosTransType");
    MAP_FIELD_CODE_TO_NAME.put("710","PosReqID");
    MAP_FIELD_CODE_TO_NAME.put("711","NoUnderlyings");
    MAP_FIELD_CODE_TO_NAME.put("712","PosMaintAction");
    MAP_FIELD_CODE_TO_NAME.put("713","OrigPosReqRefID");
    MAP_FIELD_CODE_TO_NAME.put("714","PosMaintRptRefID");
    MAP_FIELD_CODE_TO_NAME.put("715","ClearingBusinessDate");
    MAP_FIELD_CODE_TO_NAME.put("716","SettlSessID");
    MAP_FIELD_CODE_TO_NAME.put("717","SettlSessSubID");
    MAP_FIELD_CODE_TO_NAME.put("718","AdjustmentType");
    MAP_FIELD_CODE_TO_NAME.put("719","ContraryInstructionIndicator");
    MAP_FIELD_CODE_TO_NAME.put("720","PriorSpreadIndicator");
    MAP_FIELD_CODE_TO_NAME.put("721","PosMaintRptID");
    MAP_FIELD_CODE_TO_NAME.put("722","PosMaintStatus");
    MAP_FIELD_CODE_TO_NAME.put("723","PosMaintResult");
    MAP_FIELD_CODE_TO_NAME.put("724","PosReqType");
    MAP_FIELD_CODE_TO_NAME.put("725","ResponseTransportType");
    MAP_FIELD_CODE_TO_NAME.put("726","ResponseDestination");
    MAP_FIELD_CODE_TO_NAME.put("727","TotalNumPosReports");
    MAP_FIELD_CODE_TO_NAME.put("728","PosReqResult");
    MAP_FIELD_CODE_TO_NAME.put("729","PosReqStatus");
    MAP_FIELD_CODE_TO_NAME.put("730","SettlPrice");
    MAP_FIELD_CODE_TO_NAME.put("731","SettlPriceType");
    MAP_FIELD_CODE_TO_NAME.put("732","UnderlyingSettlPrice");
    MAP_FIELD_CODE_TO_NAME.put("733","UnderlyingSettlPriceType");
    MAP_FIELD_CODE_TO_NAME.put("734","PriorSettlPrice");
    MAP_FIELD_CODE_TO_NAME.put("735","NoQuoteQualifiers");
    MAP_FIELD_CODE_TO_NAME.put("736","AllocSettlCurrency");
    MAP_FIELD_CODE_TO_NAME.put("737","AllocSettlCurrAmt");
    MAP_FIELD_CODE_TO_NAME.put("738","InterestAtMaturity");
    MAP_FIELD_CODE_TO_NAME.put("739","LegDatedDate");
    MAP_FIELD_CODE_TO_NAME.put("740","LegPool");
    MAP_FIELD_CODE_TO_NAME.put("741","AllocInterestAtMaturity");
    MAP_FIELD_CODE_TO_NAME.put("742","AllocAccruedInterestAmt");
    MAP_FIELD_CODE_TO_NAME.put("743","DeliveryDate");
    MAP_FIELD_CODE_TO_NAME.put("744","AssignmentMethod");
    MAP_FIELD_CODE_TO_NAME.put("745","AssignmentUnit");
    MAP_FIELD_CODE_TO_NAME.put("746","OpenInterest");
    MAP_FIELD_CODE_TO_NAME.put("747","ExerciseMethod");
    MAP_FIELD_CODE_TO_NAME.put("748","TotNumTradeReports");
    MAP_FIELD_CODE_TO_NAME.put("749","TradeRequestResult");
    MAP_FIELD_CODE_TO_NAME.put("750","TradeRequestStatus");
    MAP_FIELD_CODE_TO_NAME.put("751","TradeReportRejectReason");
    MAP_FIELD_CODE_TO_NAME.put("752","SideMultiLegReportingType");
    MAP_FIELD_CODE_TO_NAME.put("753","NoPosAmt");
    MAP_FIELD_CODE_TO_NAME.put("754","AutoAcceptIndicator");
    MAP_FIELD_CODE_TO_NAME.put("755","AllocReportID");
    MAP_FIELD_CODE_TO_NAME.put("756","NoNested2PartyIDs");
    MAP_FIELD_CODE_TO_NAME.put("757","Nested2PartyID");
    MAP_FIELD_CODE_TO_NAME.put("758","Nested2PartyIDSource");
    MAP_FIELD_CODE_TO_NAME.put("759","Nested2PartyRole");
    MAP_FIELD_CODE_TO_NAME.put("760","Nested2PartySubID");
    MAP_FIELD_CODE_TO_NAME.put("761","BencarkSecurityIDSource");
    MAP_FIELD_CODE_TO_NAME.put("762","SecuritySubType");
    MAP_FIELD_CODE_TO_NAME.put("763","UnderlyingSecuritySubType");
    MAP_FIELD_CODE_TO_NAME.put("764","LegSecuritySubType");
    MAP_FIELD_CODE_TO_NAME.put("765","AllowableOneSidednessPct");
    MAP_FIELD_CODE_TO_NAME.put("766","AllowableOneSidednessValue");
    MAP_FIELD_CODE_TO_NAME.put("767","AllowableOneSidednessCurr");
    MAP_FIELD_CODE_TO_NAME.put("768","NoTrdRegTimestamps");
    MAP_FIELD_CODE_TO_NAME.put("769","TrdRegTimestamp");
    MAP_FIELD_CODE_TO_NAME.put("770","TrdRegTimestampType");
    MAP_FIELD_CODE_TO_NAME.put("771","TrdRegTimestampOrigin");
    MAP_FIELD_CODE_TO_NAME.put("772","ConfirmRefID");
    MAP_FIELD_CODE_TO_NAME.put("773","ConfirmType");
    MAP_FIELD_CODE_TO_NAME.put("774","ConfirmRejReason");
    MAP_FIELD_CODE_TO_NAME.put("775","BookingType");
    MAP_FIELD_CODE_TO_NAME.put("776","IndividualAllocRejCode");
    MAP_FIELD_CODE_TO_NAME.put("777","SettlInstMsgID");
    MAP_FIELD_CODE_TO_NAME.put("778","NoSettlInst");
    MAP_FIELD_CODE_TO_NAME.put("779","LastUpdateTime");
    MAP_FIELD_CODE_TO_NAME.put("780","AllocSettlInstType");
    MAP_FIELD_CODE_TO_NAME.put("781","NoSettlPartyIDs");
    MAP_FIELD_CODE_TO_NAME.put("782","SettlPartyID");
    MAP_FIELD_CODE_TO_NAME.put("783","SettlPartyIDSource");
    MAP_FIELD_CODE_TO_NAME.put("784","SettlPartyRole");
    MAP_FIELD_CODE_TO_NAME.put("785","SettlPartySubID");
    MAP_FIELD_CODE_TO_NAME.put("786","SettlPartySubIDType");
    MAP_FIELD_CODE_TO_NAME.put("787","DlvyInstType");
    MAP_FIELD_CODE_TO_NAME.put("788","TerminationType");
    MAP_FIELD_CODE_TO_NAME.put("789","NextExpectedMsgSeqNum");
    MAP_FIELD_CODE_TO_NAME.put("790","OrdStatusReqID");
    MAP_FIELD_CODE_TO_NAME.put("791","SettlInstReqID");
    MAP_FIELD_CODE_TO_NAME.put("792","SettlInstReqRejCode");
    MAP_FIELD_CODE_TO_NAME.put("793","SecondaryAllocID");
    MAP_FIELD_CODE_TO_NAME.put("794","AllocReportType");
    MAP_FIELD_CODE_TO_NAME.put("795","AllocReportRefID");
    MAP_FIELD_CODE_TO_NAME.put("796","AllocCancReplaceReason");
    MAP_FIELD_CODE_TO_NAME.put("797","CopyMsgIndicator");
    MAP_FIELD_CODE_TO_NAME.put("798","AllocAccountType");
    MAP_FIELD_CODE_TO_NAME.put("799","OrderAvgPx");
    MAP_FIELD_CODE_TO_NAME.put("800","OrderBookingQty");
    MAP_FIELD_CODE_TO_NAME.put("801","NoSettlPartySubIDs");
    MAP_FIELD_CODE_TO_NAME.put("802","NoPartySubIDs");
    MAP_FIELD_CODE_TO_NAME.put("803","PartySubIDType");
    MAP_FIELD_CODE_TO_NAME.put("804","NoNestedPartySubIDs");
    MAP_FIELD_CODE_TO_NAME.put("805","NestedPartySubIDType");
    MAP_FIELD_CODE_TO_NAME.put("806","NoNested2PartySubIDs");
    MAP_FIELD_CODE_TO_NAME.put("807","Nested2PartySubIDType");
    MAP_FIELD_CODE_TO_NAME.put("808","AllocIntermedReqType");
    MAP_FIELD_CODE_TO_NAME.put("810","UnderlyingPx");
    MAP_FIELD_CODE_TO_NAME.put("811","PriceDelta");
    MAP_FIELD_CODE_TO_NAME.put("812","ApplQueueMax");
    MAP_FIELD_CODE_TO_NAME.put("813","ApplQueueDepth");
    MAP_FIELD_CODE_TO_NAME.put("814","ApplQueueResolution");
    MAP_FIELD_CODE_TO_NAME.put("815","ApplQueueAction");
    MAP_FIELD_CODE_TO_NAME.put("816","NoAltMDSource");
    MAP_FIELD_CODE_TO_NAME.put("817","AltMDSourceID");
    MAP_FIELD_CODE_TO_NAME.put("818","SecondaryTradeReportID");
    MAP_FIELD_CODE_TO_NAME.put("819","AvgPxIndicator");
    MAP_FIELD_CODE_TO_NAME.put("820","TradeLinkID");
    MAP_FIELD_CODE_TO_NAME.put("821","OrderInputDevice");
    MAP_FIELD_CODE_TO_NAME.put("822","UnderlyingTradingSessionID");
    MAP_FIELD_CODE_TO_NAME.put("823","UnderlyingTradingSessionSubID");
    MAP_FIELD_CODE_TO_NAME.put("824","TradeLegRefID");
    MAP_FIELD_CODE_TO_NAME.put("825","ExchangeRule");
    MAP_FIELD_CODE_TO_NAME.put("826","TradeAllocIndicator");
    MAP_FIELD_CODE_TO_NAME.put("827","ExpirationCycle");
    MAP_FIELD_CODE_TO_NAME.put("828","TrdType");
    MAP_FIELD_CODE_TO_NAME.put("829","TrdSubType");
    MAP_FIELD_CODE_TO_NAME.put("830","TransferReason");
    MAP_FIELD_CODE_TO_NAME.put("831","AsgnReqID");
    MAP_FIELD_CODE_TO_NAME.put("832","TotNumAssignmentReports");
    MAP_FIELD_CODE_TO_NAME.put("833","AsgnRptID");
    MAP_FIELD_CODE_TO_NAME.put("834","ThresholdAmount");
    MAP_FIELD_CODE_TO_NAME.put("835","PegMoveType");
    MAP_FIELD_CODE_TO_NAME.put("836","PegOffsetType");
    MAP_FIELD_CODE_TO_NAME.put("837","PegLimitType");
    MAP_FIELD_CODE_TO_NAME.put("838","PegRoundDirection");
    MAP_FIELD_CODE_TO_NAME.put("839","PeggedPrice");
    MAP_FIELD_CODE_TO_NAME.put("840","PegScope");
    MAP_FIELD_CODE_TO_NAME.put("841","DiscretionMoveType");
    MAP_FIELD_CODE_TO_NAME.put("842","DiscretionOffsetType");
    MAP_FIELD_CODE_TO_NAME.put("843","DiscretionLimitType");
    MAP_FIELD_CODE_TO_NAME.put("844","DiscretionRoundDirection");
    MAP_FIELD_CODE_TO_NAME.put("845","DiscretionPrice");
    MAP_FIELD_CODE_TO_NAME.put("846","DiscretionScope");
    MAP_FIELD_CODE_TO_NAME.put("847","TargetStrategy");
    MAP_FIELD_CODE_TO_NAME.put("848","TargetStrategyParameters");
    MAP_FIELD_CODE_TO_NAME.put("849","ParticipationRate");
    MAP_FIELD_CODE_TO_NAME.put("850","TargetStrategyPerformance");
    MAP_FIELD_CODE_TO_NAME.put("851","LastLiquidityInd");
    MAP_FIELD_CODE_TO_NAME.put("852","PublishTrdIndicator");
    MAP_FIELD_CODE_TO_NAME.put("853","ShortSaleReason");
    MAP_FIELD_CODE_TO_NAME.put("854","QtyType");
    MAP_FIELD_CODE_TO_NAME.put("855","SecondaryTrdType");
    MAP_FIELD_CODE_TO_NAME.put("856","TradeReportType");
    MAP_FIELD_CODE_TO_NAME.put("857","AllocNoOrdersType");
    MAP_FIELD_CODE_TO_NAME.put("858","SharedCommission");
    MAP_FIELD_CODE_TO_NAME.put("859","ConfirmReqID");
    MAP_FIELD_CODE_TO_NAME.put("860","AvgParPx");
    MAP_FIELD_CODE_TO_NAME.put("861","ReportedPx");
    MAP_FIELD_CODE_TO_NAME.put("862","NoCapacities");
    MAP_FIELD_CODE_TO_NAME.put("863","OrderCapacityQty");
    MAP_FIELD_CODE_TO_NAME.put("864","NoEvents");
    MAP_FIELD_CODE_TO_NAME.put("865","EventType");
    MAP_FIELD_CODE_TO_NAME.put("866","EventDate");
    MAP_FIELD_CODE_TO_NAME.put("867","EventPx");
    MAP_FIELD_CODE_TO_NAME.put("868","EventText");
    MAP_FIELD_CODE_TO_NAME.put("869","PctAtRisk");
    MAP_FIELD_CODE_TO_NAME.put("870","NoInstrAttrib");
    MAP_FIELD_CODE_TO_NAME.put("871","InstrAttribType");
    MAP_FIELD_CODE_TO_NAME.put("872","InstrAttribValue");
    MAP_FIELD_CODE_TO_NAME.put("873","DatedDate");
    MAP_FIELD_CODE_TO_NAME.put("874","InterestAccrualDate");
    MAP_FIELD_CODE_TO_NAME.put("875","CPProgram");
    MAP_FIELD_CODE_TO_NAME.put("876","CPRegType");
    MAP_FIELD_CODE_TO_NAME.put("877","UnderlyingCPProgram");
    MAP_FIELD_CODE_TO_NAME.put("878","UnderlyingCPRegType");
    MAP_FIELD_CODE_TO_NAME.put("879","UnderlyingQty");
    MAP_FIELD_CODE_TO_NAME.put("880","TrdMatchID");
    MAP_FIELD_CODE_TO_NAME.put("881","SecondaryTradeReportRefID");
    MAP_FIELD_CODE_TO_NAME.put("882","UnderlyingDirtyPrice");
    MAP_FIELD_CODE_TO_NAME.put("883","UnderlyingEndPrice");
    MAP_FIELD_CODE_TO_NAME.put("884","UnderlyingStartValue");
    MAP_FIELD_CODE_TO_NAME.put("885","UnderlyingCurrentValue");
    MAP_FIELD_CODE_TO_NAME.put("886","UnderlyingEndValue");
    MAP_FIELD_CODE_TO_NAME.put("887","NoUnderlyingStips");
    MAP_FIELD_CODE_TO_NAME.put("888","UnderlyingStipType");
    MAP_FIELD_CODE_TO_NAME.put("889","UnderlyingStipValue");
    MAP_FIELD_CODE_TO_NAME.put("890","MaturityNetMoney");
    MAP_FIELD_CODE_TO_NAME.put("891","MiscFeeBasis");
    MAP_FIELD_CODE_TO_NAME.put("892","TotNoAllocs");
    MAP_FIELD_CODE_TO_NAME.put("893","LastFragment");
    MAP_FIELD_CODE_TO_NAME.put("894","CollReqID");
    MAP_FIELD_CODE_TO_NAME.put("895","CollAsgnReason");
    MAP_FIELD_CODE_TO_NAME.put("896","CollInquiryQualifier");
    MAP_FIELD_CODE_TO_NAME.put("897","NoTrades");
    MAP_FIELD_CODE_TO_NAME.put("898","MarginRatio");
    MAP_FIELD_CODE_TO_NAME.put("899","MarginExcess");
    MAP_FIELD_CODE_TO_NAME.put("900","TotalNetValue");
    MAP_FIELD_CODE_TO_NAME.put("901","CashOutstanding");
    MAP_FIELD_CODE_TO_NAME.put("902","CollAsgnID");
    MAP_FIELD_CODE_TO_NAME.put("903","CollAsgnTransType");
    MAP_FIELD_CODE_TO_NAME.put("904","CollRespID");
    MAP_FIELD_CODE_TO_NAME.put("905","CollAsgnRespType");
    MAP_FIELD_CODE_TO_NAME.put("906","CollAsgnRejectReason");
    MAP_FIELD_CODE_TO_NAME.put("907","CollAsgnRefID");
    MAP_FIELD_CODE_TO_NAME.put("908","CollRptID");
    MAP_FIELD_CODE_TO_NAME.put("909","CollInquiryID");
    MAP_FIELD_CODE_TO_NAME.put("910","CollStatus");
    MAP_FIELD_CODE_TO_NAME.put("911","TotNumReports");
    MAP_FIELD_CODE_TO_NAME.put("912","LastRptRequested");
    MAP_FIELD_CODE_TO_NAME.put("913","AgreementDesc");
    MAP_FIELD_CODE_TO_NAME.put("914","AgreementID");
    MAP_FIELD_CODE_TO_NAME.put("915","AgreementDate");
    MAP_FIELD_CODE_TO_NAME.put("916","StartDate");
    MAP_FIELD_CODE_TO_NAME.put("917","EndDate");
    MAP_FIELD_CODE_TO_NAME.put("918","AgreementCurrency");
    MAP_FIELD_CODE_TO_NAME.put("919","DeliveryType");
    MAP_FIELD_CODE_TO_NAME.put("920","EndAccruedInterestAmt");
    MAP_FIELD_CODE_TO_NAME.put("921","StartCash");
    MAP_FIELD_CODE_TO_NAME.put("922","EndCash");
    MAP_FIELD_CODE_TO_NAME.put("923","UserRequestID");
    MAP_FIELD_CODE_TO_NAME.put("924","UserRequestType");
    MAP_FIELD_CODE_TO_NAME.put("925","NewPassword");
    MAP_FIELD_CODE_TO_NAME.put("926","UserStatus");
    MAP_FIELD_CODE_TO_NAME.put("927","UserStatusText");
    MAP_FIELD_CODE_TO_NAME.put("928","StatusValue");
    MAP_FIELD_CODE_TO_NAME.put("929","StatusText");
    MAP_FIELD_CODE_TO_NAME.put("930","RefCompID");
    MAP_FIELD_CODE_TO_NAME.put("931","RefSubID");
    MAP_FIELD_CODE_TO_NAME.put("932","NetworkResponseID");
    MAP_FIELD_CODE_TO_NAME.put("933","NetworkRequestID");
    MAP_FIELD_CODE_TO_NAME.put("934","LastNetworkResponseID");
    MAP_FIELD_CODE_TO_NAME.put("935","NetworkRequestType");
    MAP_FIELD_CODE_TO_NAME.put("936","NoCompIDs");
    MAP_FIELD_CODE_TO_NAME.put("937","NetworkStatusResponseType");
    MAP_FIELD_CODE_TO_NAME.put("938","NoCollInquiryQualifier");
    MAP_FIELD_CODE_TO_NAME.put("939","TrdRptStatus");
    MAP_FIELD_CODE_TO_NAME.put("940","AffirmStatus");
    MAP_FIELD_CODE_TO_NAME.put("941","UnderlyingStrikeCurrency");
    MAP_FIELD_CODE_TO_NAME.put("942","LegStrikeCurrency");
    MAP_FIELD_CODE_TO_NAME.put("943","TimeBracket");
    MAP_FIELD_CODE_TO_NAME.put("944","CollAction");
    MAP_FIELD_CODE_TO_NAME.put("945","CollInquiryStatus");
    MAP_FIELD_CODE_TO_NAME.put("946","CollInquiryResult");
    MAP_FIELD_CODE_TO_NAME.put("947","StrikeCurrency");
    MAP_FIELD_CODE_TO_NAME.put("948","NoNested3PartyIDs");
    MAP_FIELD_CODE_TO_NAME.put("949","Nested3PartyID");
    MAP_FIELD_CODE_TO_NAME.put("950","Nested3PartyIDSource");
    MAP_FIELD_CODE_TO_NAME.put("951","Nested3PartyRole");
    MAP_FIELD_CODE_TO_NAME.put("952","NoNested3PartySubIDs");
    MAP_FIELD_CODE_TO_NAME.put("953","Nested3PartySubID");
    MAP_FIELD_CODE_TO_NAME.put("954","Nested3PartySubIDType");
    MAP_FIELD_CODE_TO_NAME.put("955","LegContractSettlMonth");
    MAP_FIELD_CODE_TO_NAME.put("956","LegInterestAccrualDate");
  }

  public static final Map<String, Map<String, String>> MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME = new LinkedHashMap<>();
  static {
    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("B", "Buy");
      map.put("S", "Sell");
      map.put("X", "Cross");
      map.put("T", "Trade");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("4", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("N", "New");
      map.put("C", "Cancel");
      map.put("R", "Replace");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("5", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("1", "PerUnit");
      map.put("2", "Percentage");
      map.put("3", "Absolute");
      map.put("4", "PercentageWaivedCashDiscount");
      map.put("5", "PercentageWaivedEnhancedUnits");
      map.put("6", "PointsPerBondOrContract");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("13", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("1", "Not held");
      map.put("2", "Work");
      map.put("3", "Go along");
      map.put("4", "Over the day");
      map.put("5", "Held");
      map.put("6", "Participate don't initiate");
      map.put("7", "Strict scale");
      map.put("8", "Try to scale");
      map.put("9", "Stay on bidside");
      map.put("0", "Stay on offerside");
      map.put("A", "No cross");
      map.put("B", "OK to cross");
      map.put("C", "Call first");
      map.put("D", "Percent of volume");
      map.put("E", "Do not increase");
      map.put("F", "Do not reduce");
      map.put("G", "All or none");
      map.put("H", "Reinstate on System Failure");
      map.put("I", "Institutions only");
      map.put("J", "Reinstate on Trading Halt");
      map.put("K", "Cancel on Trading Halt");
      map.put("L", "Last peg");
      map.put("M", "Mid-price peg");
      map.put("N", "Non-negotiable");
      map.put("O", "Opening peg");
      map.put("P", "Market peg");
      map.put("Q", "Cancel on System Failure");
      map.put("R", "Primary peg");
      map.put("S", "Suspend");
      map.put("T", "Replaced");
      map.put("U", "Customer Display Instruction");
      map.put("V", "Netting");
      map.put("W", "Peg to VWAP");
      map.put("X", "Trade Along");
      map.put("Y", "Try to Stop");
      map.put("Z", "Cancel if Not Best");
      map.put("a", "Trailing Stop Peg");
      map.put("b", "Strict Limit");
      map.put("c", "Ignore Price Validity Checks");
      map.put("d", "Peg to Limit Price");
      map.put("e", "Work to Target Strategy");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("18", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "New");
      map.put("1", "Cancel");
      map.put("2", "Correct");
      map.put("3", "Status");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("20", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("1", "Automated execution order, private, no Broker intervention");
      map.put("2", "Automated execution order, public, Broker intervention OK");
      map.put("3", "Manual order, best execution");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("21", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("1", "CUSIP");
      map.put("2", "SEDOL");
      map.put("3", "QUIK");
      map.put("4", "ISIN");
      map.put("5", "RIC");
      map.put("6", "ISO Currency");
      map.put("7", "ISO Country");
      map.put("8", "Exchange Symbol");
      map.put("9", "CTA");
      map.put("A", "Bloomberg Symbol");
      map.put("B", "Wertpapier");
      map.put("C", "Dutch");
      map.put("D", "Valoren");
      map.put("E", "Sicovam");
      map.put("F", "Belgian");
      map.put("G", "Common");
      map.put("H", "Clearing House");
      map.put("I", "ISDA");
      map.put("J", "Options Price Reporting Authority");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("22", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("L", "Low");
      map.put("M", "Medium");
      map.put("H", "High");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("25", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "1000000000");
      map.put("L", "Low");
      map.put("M", "Medium");
      map.put("H", "High");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("27", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("N", "New");
      map.put("C", "Cancel");
      map.put("R", "Replace");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("28", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("1", "Agent");
      map.put("2", "Cross as agent");
      map.put("3", "Cross as principal");
      map.put("4", "Principal");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("29", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "Heartbeat");
      map.put("1", "Test Request");
      map.put("2", "Resend Request");
      map.put("3", "Reject");
      map.put("4", "Sequence Reset");
      map.put("5", "Logout");
      map.put("6", "Indication of Interest");
      map.put("7", "Advertisement");
      map.put("8", "Execution Report");
      map.put("9", "Order Cancel Reject");
      map.put("A", "Logon");
      map.put("B", "News");
      map.put("C", "Email");
      map.put("D", "New Order Single");
      map.put("E", "New Order List");
      map.put("F", "Order Cancel Request");
      map.put("G", "Order Cancel/Replace Request");
      map.put("H", "Order Status Request");
      map.put("J", "Allocation Instruction");
      map.put("K", "List Cancel Request");
      map.put("L", "List Execute");
      map.put("M", "List Status Request");
      map.put("N", "List Status");
      map.put("P", "Allocation Instruction Ack");
      map.put("Q", "Don't Know Trade");
      map.put("R", "Quote Request");
      map.put("S", "Quote");
      map.put("T", "Settlement Instructions");
      map.put("V", "Market Data Request");
      map.put("W", "Market Data-Snapshot/Full Refresh");
      map.put("X", "Market Data-Incremental Refresh");
      map.put("Y", "Market Data Request Reject");
      map.put("Z", "Quote Cancel");
      map.put("a", "Quote Status Request");
      map.put("b", "Mass Quote Acknowledgement");
      map.put("c", "Security Definition Request");
      map.put("d", "Security Definition");
      map.put("e", "Security Status Request");
      map.put("f", "Security Status");
      map.put("g", "Trading Session Status Request");
      map.put("h", "Trading Session Status");
      map.put("i", "Mass Quote");
      map.put("j", "Business Message Reject");
      map.put("k", "Bid Request");
      map.put("l", "Bid Response");
      map.put("m", "List Strike Price");
      map.put("n", "XML message");
      map.put("o", "Registration Instructions");
      map.put("p", "Registration Instructions Response");
      map.put("q", "Order Mass Cancel Request");
      map.put("r", "Order Mass Cancel Report");
      map.put("s", "New Order Cross");
      map.put("t", "Cross Order Cancel/Replace Request");
      map.put("u", "Cross Order Cancel Request");
      map.put("v", "Security Type Request");
      map.put("w", "Security Types");
      map.put("x", "Security List Request");
      map.put("y", "Security List");
      map.put("z", "Derivative Security List Request");
      map.put("AA", "Derivative Security List");
      map.put("AB", "New Order Multileg");
      map.put("AC", "Multileg Order Cancel/Replace");
      map.put("AD", "Trade Capture Report Request");
      map.put("AE", "Trade Capture Report");
      map.put("AF", "Order Mass Status Request");
      map.put("AG", "Quote Request Reject");
      map.put("AH", "RFQ Request");
      map.put("AI", "Quote Status Report");
      map.put("AJ", "Quote Response");
      map.put("AK", "Confirmation");
      map.put("AL", "Position Maintenance Request");
      map.put("AM", "Position Maintenance Report");
      map.put("AN", "Request For Positions");
      map.put("AO", "Request For Positions Ack");
      map.put("AP", "Position Report");
      map.put("AQ", "Trade Capture Report Request Ack");
      map.put("AR", "Trade Capture Report Ack");
      map.put("AS", "Allocation Report");
      map.put("AT", "Allocation Report Ack");
      map.put("AU", "Confirmation Ack");
      map.put("AV", "Settlement Instruction Request");
      map.put("AW", "Assignment Report");
      map.put("AX", "Collateral Request");
      map.put("AY", "Collateral Assignment");
      map.put("AZ", "Collateral Response");
      map.put("BA", "Collateral Report");
      map.put("BB", "Collateral Inquiry");
      map.put("BC", "Network (Counterparty System) Status Request");
      map.put("BD", "Network (Counterparty System) Status Response");
      map.put("BE", "User Request");
      map.put("BF", "User Response");
      map.put("BG", "Collateral Inquiry Ack");
      map.put("BH", "Confirmation Request");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("35", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "New");
      map.put("1", "Partially filled");
      map.put("2", "Filled");
      map.put("3", "Done for day");
      map.put("4", "Canceled");
      map.put("5", "Removed/Canceled");
      map.put("6", "Pending Cancel");
      map.put("7", "Stopped");
      map.put("8", "Rejected");
      map.put("9", "Suspended");
      map.put("A", "Pending New");
      map.put("B", "Calculated");
      map.put("C", "Expired");
      map.put("D", "Accepted for bidding");
      map.put("E", "Pending Replace");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("39", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("1", "Market");
      map.put("2", "Limit");
      map.put("3", "Stop");
      map.put("4", "Stop limit");
      map.put("5", "Market on close");
      map.put("6", "With or without");
      map.put("7", "Limit or better");
      map.put("8", "Limit with or without");
      map.put("9", "On basis");
      map.put("A", "On close");
      map.put("B", "Limit on close");
      map.put("C", "Forex - Market");
      map.put("D", "Previously quoted");
      map.put("E", "Previously indicated");
      map.put("F", "Forex - Limit");
      map.put("G", "Forex - Swap");
      map.put("H", "Forex - Previously Quoted");
      map.put("I", "Funari");
      map.put("J", "Market If Touched");
      map.put("K", "Market with Leftover as Limit");
      map.put("L", "Previous Fund Valuation Point");
      map.put("M", "Next Fund Valuation Point");
      map.put("P", "Pegged");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("40", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("1", "Buy");
      map.put("2", "Sell");
      map.put("3", "Buy minus");
      map.put("4", "Sell plus");
      map.put("5", "Sell short");
      map.put("6", "Sell short exempt");
      map.put("7", "Undisclosed");
      map.put("8", "Cross");
      map.put("9", "Cross short");
      map.put("A", "Cross short exempt");
      map.put("B", "As Defined");
      map.put("C", "Opposite");
      map.put("D", "Subscribe");
      map.put("E", "Redeem");
      map.put("F", "Lend");
      map.put("G", "Borrow");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("54", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "Day");
      map.put("1", "Good Till Cancel");
      map.put("2", "At the Opening");
      map.put("3", "Immediate or Cancel");
      map.put("4", "Fill or Kill");
      map.put("5", "Good Till Crossing");
      map.put("6", "Good Till Date");
      map.put("7", "At the Close");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("59", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "Normal");
      map.put("1", "Flash");
      map.put("2", "Background");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("61", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "Regular");
      map.put("1", "Cash");
      map.put("2", "Next Day");
      map.put("3", "T+2");
      map.put("4", "T+3");
      map.put("5", "T+4");
      map.put("6", "Future");
      map.put("7", "When And If Issued");
      map.put("8", "Sellers Option");
      map.put("9", "T+5");
      map.put("A", "T+1");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("63", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "New");
      map.put("1", "Replace");
      map.put("2", "Cancel");
      map.put("3", "Preliminary");
      map.put("4", "Calculated");
      map.put("5", "Calculated without Preliminary");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("71", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "regular");
      map.put("1", "soft dollar");
      map.put("2", "step-in");
      map.put("3", "step-out");
      map.put("4", "soft-dollar step-in");
      map.put("5", "soft-dollar step-out");
      map.put("6", "plan sponsor");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("81", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "accepted ");
      map.put("1", "block level reject");
      map.put("2", "account level reject");
      map.put("3", "received ");
      map.put("4", "incomplete");
      map.put("5", "rejected by intermediary");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("87", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "unknown account(s)");
      map.put("1", "incorrect quantity");
      map.put("2", "incorrect average price");
      map.put("3", "unknown executing broker mnemonic");
      map.put("4", "commission difference");
      map.put("5", "unknown OrderID <37>");
      map.put("6", "unknown ListID <66>");
      map.put("7", "other");
      map.put("8", "incorrect allocated quantity");
      map.put("9", "calculation difference");
      map.put("10", "unknown or stale ExecID <17>");
      map.put("11", "mismatched data value");
      map.put("12", "unknown ClOrdID <11>");
      map.put("13", "warehouse request rejected");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("88", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "New");
      map.put("1", "Reply");
      map.put("2", "Admin Reply");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("94", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("Y", "Possible resend");
      map.put("N", "Original transmission");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("97", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "None / other");
      map.put("1", "PKCS");
      map.put("2", "DES (ECB mode)");
      map.put("3", "PKCS/DES");
      map.put("4", "PGP/DES");
      map.put("5", "PGP/DES-MD5");
      map.put("6", "PEM/DES-MD5");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("98", map);
    }

    // FIELD "100" see https://www.onixs.biz/fix-dictionary/4.4/app_6_c.html

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "Too late to cancel");
      map.put("1", "Unknown order");
      map.put("2", "Broker / Exchange Option");
      map.put("3", "Order already in Pending Cancel or Pending Replace status");
      map.put("4", "Unable to process Order Mass Cancel Request <q>");
      map.put("5", " OrigOrdModTime <586> did not match last TransactTime <60> of order");
      map.put("6", "Duplicate ClOrdID <11> received");
      map.put("99", "Other");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("102", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "Broker / Exchange option");
      map.put("1", "Unknown symbol");
      map.put("2", "Exchange closed");
      map.put("3", "Order exceeds limit");
      map.put("4", "Too late to enter");
      map.put("5", "Unknown Order");
      map.put("6", "Duplicate Order");
      map.put("7", "Duplicate of a verbally communicated order");
      map.put("8", "Stale Order");
      map.put("9", "Trade Along required");
      map.put("10", "Invalid Investor ID");
      map.put("11", "Unsupported order characteristic");
      map.put("12", "Surveillence Option");
      map.put("13", "Incorrect quantity");
      map.put("14", "Incorrect allocated quantity");
      map.put("15", "Unknown account(s)");
      map.put("99", "Other");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("103", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("A", "All or none");
      map.put("B", "Market On Close");
      map.put("C", "At the Close");
      map.put("D", "VWAP");
      map.put("I", "In touch with");
      map.put("L", "Limit");
      map.put("M", "More behind");
      map.put("O", "At the open");
      map.put("P", "Taking a position");
      map.put("Q", "At the Market");
      map.put("R", "Ready to trade");
      map.put("S", "Portfolio shown");
      map.put("T", "Through the day");
      map.put("V", "Versus");
      map.put("W", "Indication - Working away");
      map.put("X", "Crossing opportunity");
      map.put("Y", "At the Midpoint");
      map.put("Z", "Pre-open");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("104", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("A", "Unknown symbol");
      map.put("B", "Wrong side");
      map.put("C", "Quantity exceeds order");
      map.put("D", "No matching order");
      map.put("E", "Price <44> exceeds limit");
      map.put("F", "Calculation difference");
      map.put("Z", "Other");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("127", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("1", "Regulatory");
      map.put("2", "Tax");
      map.put("3", "Local Commission");
      map.put("4", "Exchange Fees");
      map.put("5", "Stamp");
      map.put("6", "Levy");
      map.put("7", "Other");
      map.put("8", "Markup");
      map.put("9", "Consumption Tax");
      map.put("10", "Per transaction");
      map.put("11", "Conversion");
      map.put("12", "Agent");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("139", map);
    }

    // Field "149" see https://www.onixs.biz/fix-dictionary/4.4/app_6_b.html

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "New");
      map.put("1", "Partial fill");
      map.put("2", "Fill");
      map.put("3", "Done for day");
      map.put("4", "Canceled");
      map.put("5", "Replaced");
      map.put("6", "Pending Cancel");
      map.put("7", "Stopped");
      map.put("8", "Rejected");
      map.put("9", "Suspended");
      map.put("A", "Pending New");
      map.put("B", "Calculated");
      map.put("C", "Expired");
      map.put("D", "Restated ");
      map.put("E", "Pending Replace");
      map.put("F", "Trade ");
      map.put("G", "Trade Correct");
      map.put("H", "Trade Cancel");
      map.put("I", "Order Status");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("150", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("M", "Multiply");
      map.put("D", "Divide");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("155", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "Default");
      map.put("1", "Standing Instructions Provided");
      map.put("2", "Specific Allocation Account Overriding");
      map.put("3", "Specific Allocation Account Standing");
      map.put("4", "Specific Order for a single account");
      map.put("5", "Request reject");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("160", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("N", "New");
      map.put("C", "Cancel");
      map.put("R", "Replace");
      map.put("T", "Restate");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("163", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("1", "Broker's Instructions");
      map.put("2", "Institution's Instructions");
      map.put("3", "Investor");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("165", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "Other");
      map.put("1", "DTC SID");
      map.put("2", "Thomson ALERT");
      map.put("3", "A Global Custodian");
      map.put("4", "AccountNet");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("169", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "Versus Payment");
      map.put("1", "Free");
      map.put("2", "Tri-Party");
      map.put("3", "Hold In Custody");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("172", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "F/X Netting");
      map.put("1", "F/X Swap");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("197", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "Put");
      map.put("1", "Call");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("201", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "Covered");
      map.put("1", "Uncovered");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("203", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "Customer");
      map.put("1", "Firm");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("204", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("L", "Long");
      map.put("S", "Short");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("206", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("1", "Match");
      map.put("2", "Forward");
      map.put("3", "Forward and Match");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("209", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("1", "Target Firm");
      map.put("2", "Target List");
      map.put("3", "Block Firm");
      map.put("4", "Block List");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("216", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("1", "CURVE");
      map.put("2", "5-YR");
      map.put("3", "OLD-5");
      map.put("4", "10-YR");
      map.put("5", "OLD-10");
      map.put("6", "30-YR");
      map.put("7", "OLD-30");
      map.put("8", "3-MO-LIBOR");
      map.put("9", "6-MO-LIBOR");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("219", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "Snapshot");
      map.put("1", "Snapshot + Updates");
      map.put("2", "Disable previous Snapshot + Update Request");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("263", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "Full Book");
      map.put("1", "Top of Book");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("264", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "Full Refresh");
      map.put("1", "Incremental Refresh");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("265", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "Bid");
      map.put("1", "Offer");
      map.put("2", "Trade");
      map.put("3", "Index Value");
      map.put("4", "Opening Price");
      map.put("5", "Closing Price");
      map.put("6", "Settlement Price");
      map.put("7", "Trading Session High Price");
      map.put("8", "Trading Session Low Price");
      map.put("9", "Trading Session VWAP Price");
      map.put("A", "Imbalance");
      map.put("B", "Trade Volume");
      map.put("C", "Open Interest");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("269", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "Plus Tick");
      map.put("1", "Zero-Plus Tick");
      map.put("2", "Minus Tick");
      map.put("3", "Zero-Minus Tick");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("274", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("A", "Open / Active");
      map.put("B", "Closed / Inactive");
      map.put("C", "Exchange Best");
      map.put("D", "Consolidated Best");
      map.put("E", "Locked");
      map.put("F", "Crossed");
      map.put("G", "Depth");
      map.put("H", "Fast Trading");
      map.put("I", "Non-Firm");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("276", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("A", "Cash (only) Market");
      map.put("B", "Average Price Trade");
      map.put("C", "Cash Trade (same day clearing)");
      map.put("D", "Next Day (only) Market");
      map.put("E", "Opening / Reopening Trade Detail");
      map.put("F", "Intraday Trade Detail");
      map.put("G", "Rule 127 Trade (NYSE)");
      map.put("H", "Rule 155 Trade (Amex)");
      map.put("I", "Sold Last (late reporting)");
      map.put("J", "Next Day Trade (next day clearing)");
      map.put("K", "Opened (late report of opened trade)");
      map.put("L", "Seller");
      map.put("M", "Sold (out of sequence)");
      map.put("N", "Stopped Stock (guarantee of price but does not execute the order)");
      map.put("P", "Imbalance More Buyers");
      map.put("Q", "Imbalance More Sellers");
      map.put("R", "Opening Price");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("277", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "New");
      map.put("1", "Change");
      map.put("2", "Delete");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("279", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "Unknown symbol");
      map.put("1", "Duplicate MDReqID <262>");
      map.put("2", "Insufficient Bandwidth");
      map.put("3", "Insufficient Permissions");
      map.put("4", "Unsupported SubscriptionRequestType <263>");
      map.put("5", "Unsupported MarketDepth <264>");
      map.put("6", "Unsupported MDUpdateType <265>");
      map.put("7", "Unsupported AggregatedBook <266>");
      map.put("8", "Unsupported MDEntryType <269>");
      map.put("9", "Unsupported TradingSessionID <336>");
      map.put("A", "Unsupported Scope <546>");
      map.put("B", "Unsupported OpenCloseSettlFlag <286>");
      map.put("C", "Unsupported MDImplicitDelete <547>");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("281", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "Cancelation / Trade Bust");
      map.put("1", "Error");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("285", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "Daily Open / Close / Settlement entry");
      map.put("1", "Session Open / Close / Settlement entry");
      map.put("2", "Delivery Settlement entry");
      map.put("3", "Expected entry");
      map.put("4", "Entry from previous business day");
      map.put("5", "Theoretical Price value");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("286", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("1", "Bankrupt");
      map.put("2", "Pending delisting");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("291", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("A", "Ex-Dividend");
      map.put("B", "Ex-Distribution");
      map.put("C", "Ex-Rights");
      map.put("D", "New");
      map.put("E", "Ex-Interest");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("292", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "Accepted");
      map.put("1", "Canceled for Symbol <55>");
      map.put("2", "Canceled for SecurityType <167>");
      map.put("3", "Canceled for UnderlyingSymbol <311>");
      map.put("4", "Canceled All");
      map.put("5", "Rejected");
      map.put("6", "Removed from Market");
      map.put("7", "Expired");
      map.put("8", "Query");
      map.put("9", "Quote Not Found");
      map.put("10", "Pending");
      map.put("11", "Pass");
      map.put("12", "Locked Market Warning");
      map.put("13", "Cross Market Warning");
      map.put("14", "Canceled due to lock market");
      map.put("15", "Canceled due to cross market");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("297", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("1", "Cancel for Symbol <55>");
      map.put("2", "Cancel for SecurityType <167>");
      map.put("3", "Cancel for UnderlyingSymbol <311>");
      map.put("4", "Cancel All Quotes");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("298", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("1", "Unknown symbol (Security)");
      map.put("2", "Exchange(Security) closed");
      map.put("3", "Quote Request exceeds limit");
      map.put("4", "Too late to enter");
      map.put("5", "Unknown Quote");
      map.put("6", "Duplicate Quote");
      map.put("7", "Invalid bid/ask spread");
      map.put("8", "Invalid price");
      map.put("9", "Not authorized to quote security");
      map.put("99", "Other");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("300", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "No Acknowledgement");
      map.put("1", "Acknowledge only negative or erroneous quotes");
      map.put("2", "Acknowledge each Quote messages");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("301", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("1", "Manual");
      map.put("2", "Automatic");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("303", map);
    }

    {
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("305", MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.get("22"));
    }

    {
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("317", MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.get("206"));
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("1", "Accept security proposal as is");
      map.put("2", "Accept security proposal with revisions as indicated in the message");
      map.put("3", "List of security types returned per request");
      map.put("4", "List of securities returned per request");
      map.put("5", "Reject security proposal");
      map.put("6", "Can not match selection criteria");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("323", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("1", "Opening Delay");
      map.put("2", "Trading Halt");
      map.put("3", "Resume");
      map.put("4", "No Open/No Resume");
      map.put("5", "Price Indication");
      map.put("6", "Trading Range Indication");
      map.put("7", "Market Imbalance Buy");
      map.put("8", "Market Imbalance Sell");
      map.put("9", "Market On Close Imbalance Buy");
      map.put("10", "Market On Close Imbalance Sell");
      map.put("12", "No Market Imbalance");
      map.put("13", "No Market On Close Imbalance");
      map.put("14", "ITS Pre-Opening");
      map.put("15", "New Price Indication");
      map.put("16", "Trade Dissemination Time");
      map.put("17", "Ready to trade (start of session)");
      map.put("18", "Not Available for trading (end of session)");
      map.put("19", "Not Traded on this Market");
      map.put("20", "Unknown or Invalid");
      map.put("21", "Pre-Open");
      map.put("22", "Opening Rotation");
      map.put("23", "Fast Market");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("326", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("I", "Order Imbalance");
      map.put("X", "Equipment Changeover");
      map.put("P", "News <B> Pending");
      map.put("D", "News <B> Dissemination");
      map.put("E", "Order Influx");
      map.put("M", "Additional Information");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("327", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("1", "Cancel");
      map.put("2", "Error");
      map.put("3", "Correction");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("334", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("1", "Electronic");
      map.put("2", "Open Outcry");
      map.put("3", "Two Party");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("338", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("1", "Testing");
      map.put("2", "Simulated");
      map.put("3", "Production");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("339", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "Unknown");
      map.put("1", "Halted");
      map.put("2", "Open");
      map.put("3", "Closed");
      map.put("4", "Pre-Open");
      map.put("5", "Pre-Close");
      map.put("6", "Request Rejected");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("340", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("1", "Unknown symbol (Security)");
      map.put("2", "Exchange(Security) closed");
      map.put("3", "Quote exceeds limit");
      map.put("4", "Too late to enter");
      map.put("5", "Unknown Quote");
      map.put("6", "Duplicate Quote");
      map.put("7", "Invalid bid/ask spread");
      map.put("8", "Invalid price");
      map.put("9", "Not authorized to quote security");
      map.put("99", "Other");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("368", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "Invalid tag number");
      map.put("1", "Required tag missing");
      map.put("2", "Tag not defined for this message type");
      map.put("3", "Undefined Tag");
      map.put("4", "Tag specified without a value");
      map.put("5", "Value is incorrect (out of range) for this tag");
      map.put("6", "Incorrect data format for value");
      map.put("7", "Decryption problem");
      map.put("8", "Signature <89> problem");
      map.put("9", "CompID problem");
      map.put("10", "SendingTime <52> accuracy problem");
      map.put("11", "Invalid MsgType <35>");
      map.put("12", "XML Validation error");
      map.put("13", "Tag appears more than once");
      map.put("14", "Tag specified out of required order");
      map.put("15", "Repeating group fields out of order");
      map.put("16", "Incorrect NumInGroup count for repeating group");
      map.put("17", "Non \"Data\" value includes field delimiter (<SOH> character)");
      map.put("99", "Other");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("373", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("N", "New");
      map.put("C", "Cancel");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("374", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "GT Corporate action");
      map.put("1", "GT renewal / restatement (no corporate action)");
      map.put("2", "Verbal change");
      map.put("3", "Repricing of order");
      map.put("4", "Broker option");
      map.put("5", "Partial decline of OrderQty <38>");
      map.put("6", "Cancel on Trading Halt");
      map.put("7", "Cancel on System Failure");
      map.put("8", "Market (Exchange) Option");
      map.put("9", "Canceled, Not Best");
      map.put("10", "Warehouse recap");
      map.put("99", "Other");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("378", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "Other");
      map.put("1", "Unkown ID");
      map.put("2", "Unknown Security");
      map.put("3", "Unsupported Message Type");
      map.put("4", "Application not available");
      map.put("5", "Conditionally Required Field Missing");
      map.put("6", "Not authorized");
      map.put("7", "DeliverTo firm not available at this time");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("380", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("S", "Send");
      map.put("R", "Receive");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("385", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "Related to displayed price");
      map.put("1", "Related to market price");
      map.put("2", "Related to primary price");
      map.put("3", "Related to local primary price");
      map.put("4", "Related to midpoint price");
      map.put("5", "Related to last trade price");
      map.put("6", "Related to VWAP");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("388", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("1", "Non Disclosed Style");
      map.put("2", "Disclosed Style");
      map.put("3", "No Bidding Process");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("394", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("1", "Sector");
      map.put("2", "Country <421>");
      map.put("3", "Index");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("399", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("1", "5 day moving average");
      map.put("2", "20 day moving average");
      map.put("3", "Normal Market Size");
      map.put("4", "Other");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("409", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("1", "BuySide");
      map.put("2", "SellSide");
      map.put("3", "Real-time");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("414", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("1", "Net");
      map.put("2", "Gross");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("416", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("R", "Risk Trade");
      map.put("G", "VWAP Guarantee");
      map.put("A", "Agency");
      map.put("J", "Guaranteed Close");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("418", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("2", "Closing Price at morning session");
      map.put("3", "Closing Price");
      map.put("4", "Current price");
      map.put("5", "SQ");
      map.put("6", "VWAP through a day");
      map.put("7", "VWAP through a morning session");
      map.put("8", "VWAP through an afternoon session");
      map.put("9", "VWAP through a day except YORI (an opening auction)");
      map.put("A", "VWAP through a morning session except YORI (an opening auction)");
      map.put("B", "VWAP through an afternoon session except YORI (an opening auction)");
      map.put("C", "Strike");
      map.put("D", "Open");
      map.put("Z", "Others");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("419", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("1", "Percentage ");
      map.put("2", "Per unit");
      map.put("3", "Fixed Amount");
      map.put("4", "Discount");
      map.put("5", "Premium");
      map.put("6", "Spread");
      map.put("7", "TED price");
      map.put("8", "TED yield");
      map.put("9", "Yield");
      map.put("10", "Fixed cabinet trade price");
      map.put("11", "Variable cabinet trade price");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("423", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "book out all trades on day of execution");
      map.put("1", "accumulate executions until order is filled or expires");
      map.put("2", "accumulate until verbally notified otherwise");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("427", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("1", "Ack");
      map.put("2", "Response");
      map.put("3", "Timed");
      map.put("4", "ExecStarted");
      map.put("5", "AllDone");
      map.put("6", "Alert");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("429", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("1", "Net");
      map.put("2", "Gross");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("430", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("1", "InBiddingProcess");
      map.put("2", "ReceivedForExecution");
      map.put("3", "Executing");
      map.put("4", "Canceling");
      map.put("5", "Alert");
      map.put("6", "All Done");
      map.put("7", "Reject");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("431", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("1", "Immediate");
      map.put("2", "Wait for Execute Instruction");
      map.put("3", "Exchange/switch CIV order - Sell driven");
      map.put("4", "Exchange/switch CIV order - Buy driven, cash top-up");
      map.put("5", "Exchange/switch CIV order - Buy driven, cash withdraw");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("433", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("1", "Order Cancel Request <F>");
      map.put("2", "Order Cancel/Replace Request <G>");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("434", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("1", "Single Security");
      map.put("2", "Individual leg of a multi-leg security");
      map.put("3", "Multi-leg security");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("442", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("B", "BIC");
      map.put("C", "Generally accepted market participant identifier");
      map.put("D", "Proprietary/Custom code");
      map.put("E", "ISO Country <421> Code");
      map.put("F", "Settlement Entity Location");
      map.put("G", "MIC");
      map.put("H", "CSD participant/member code");
      map.put("1", "Korean Investor ID");
      map.put("2", "Taiwanese Qualified Foreign Investor ID QFII / FID");
      map.put("3", "Taiwanese Trading Account");
      map.put("4", "Malaysian Central Depository (MCD) number");
      map.put("5", "Chinese B Share (Shezhen and Shanghai)");
      map.put("6", "UK National Insurance or Pension Number");
      map.put("7", "US Social Security Number");
      map.put("8", "US Employer Identification Number");
      map.put("9", "Australian Business Number");
      map.put("A", "Australian Tax File Number");
      map.put("I", "Directed broker three character acronym");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("447", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("1", "Executing Firm");
      map.put("2", "Broker of Credit");
      map.put("3", "Client ID");
      map.put("4", "Clearing Firm");
      map.put("5", "Investor ID");
      map.put("6", "Introducing Firm");
      map.put("7", "Entering Firm");
      map.put("8", "Locate/Lending Firm");
      map.put("9", "Fund manager Client ID");
      map.put("10", "Settlement Location");
      map.put("11", "Order Origination Trader");
      map.put("12", "Executing Trader");
      map.put("13", "Order Origination Firm ");
      map.put("14", "Giveup Clearing Firm");
      map.put("15", "Correspondant Clearing Firm");
      map.put("16", "Executing System");
      map.put("17", "Contra Firm");
      map.put("18", "Contra Clearing Firm");
      map.put("19", "Sponsoring Firm");
      map.put("20", "Underlying Contra Firm");
      map.put("21", "Clearing Organization");
      map.put("22", "Exchange");
      map.put("24", "Customer Account");
      map.put("25", "Correspondent Clearing Organization");
      map.put("26", "Correspondent Broker");
      map.put("27", "Buyer/Seller");
      map.put("28", "Custodian");
      map.put("29", "Intermediary");
      map.put("30", "Agent");
      map.put("31", "Sub custodian");
      map.put("32", "Beneficiary");
      map.put("33", "Interested party");
      map.put("34", "Regulatory body");
      map.put("35", "Liquidity provider");
      map.put("36", "Entering Trader");
      map.put("37", "Contra Trader");
      map.put("38", "Position Account");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("452", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("1", "AGENCY");
      map.put("2", "COMMODITY");
      map.put("3", "CORPORATE");
      map.put("4", "CURRENCY");
      map.put("5", "EQUITY");
      map.put("6", "GOVERNMENT");
      map.put("7", "INDEX");
      map.put("8", "LOAN");
      map.put("9", "MONEYMARKET");
      map.put("10", "MORTGAGE");
      map.put("11", "MUNICIPAL");
      map.put("12", "OTHER");
      map.put("13", "FINANCING");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("460", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("1", "SHARES");
      map.put("2", "BONDS");
      map.put("3", "CURRENTFACE");
      map.put("4", "ORIGINALFACE");
      map.put("5", "CURRENCY");
      map.put("6", "CONTRACTS");
      map.put("7", "OTHER");
      map.put("8", "PAR");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("465", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "Round to nearest");
      map.put("1", "Round down");
      map.put("2", "Round up");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("468", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("1", "CREST");
      map.put("2", "NSCC");
      map.put("3", "Euroclear");
      map.put("4", "Clearstream");
      map.put("5", "Cheque");
      map.put("6", "Telegraphic Transfer");
      map.put("7", "FedWire");
      map.put("8", "Direct Credit (BECS, BACS)");
      map.put("9", "ACH Credit");
      map.put("10", "BPAY");
      map.put("11", "High Value Clearing System (HVACS)");
      map.put("12", "Reinvest in fund");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("477", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("Y", "Yes");
      map.put("N", "No - execution only");
      map.put("M", "No - waiver agreement");
      map.put("O", "No - institutional");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("480", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("Y", "Passed");
      map.put("N", "Not checked");
      map.put("1", "Exempt - Below The Limit");
      map.put("2", "Exempt - Client Money Type Exemption");
      map.put("3", "Exempt - Authorised Credit or Financial Institution");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("481", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("B", "Bid price");
      map.put("C", "Creation price");
      map.put("D", "Creation price plus adjustment %");
      map.put("E", "Creation price plus adjustment amount");
      map.put("O", "Offer price");
      map.put("P", "Offer price minus adjustment %");
      map.put("Q", "Offer price minus adjustment amount");
      map.put("S", "Single price");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("484", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "New");
      map.put("1", "Cancel");
      map.put("2", "Replace");
      map.put("3", "Release");
      map.put("4", "Reverse");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("487", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("1", "CREST");
      map.put("2", "NSCC");
      map.put("3", "Euroclear");
      map.put("4", "Clearstream");
      map.put("5", "Cheque");
      map.put("6", "Telegraphic Transfer");
      map.put("7", "FedWire");
      map.put("8", "Debit Card");
      map.put("9", "Direct Debit (BECS)");
      map.put("10", "Direct Credit (BECS)");
      map.put("11", "Credit Card");
      map.put("12", "ACH Debit");
      map.put("13", "ACH Credit");
      map.put("14", "BPAY");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("492", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "None/Not Applicable (default)");
      map.put("1", "Maxi ISA (UK)");
      map.put("2", "TESSA (UK)");
      map.put("3", "Mini Cash ISA (UK)");
      map.put("4", "Mini Stocks and Shares ISA (UK)");
      map.put("5", "Mini Insurance ISA (UK)");
      map.put("6", "Current year payment (US)");
      map.put("7", "Prior year payment (US)");
      map.put("8", "Asset transfer (US)");
      map.put("9", "Employee - prior year (US)");
      map.put("10", "Employee - current year (US)");
      map.put("11", "Employer - prior year (US)");
      map.put("12", "Employer - current year (US)");
      map.put("13", "Non-fund prototype IRA (US)");
      map.put("14", "Non-fund qualified plan (US)");
      map.put("15", "Defined contribution plan (US)");
      map.put("16", "Individual Retirement Account (US)");
      map.put("17", "Individual Retirement Account - Rollover (US)");
      map.put("18", "KEOGH (US)");
      map.put("19", "Profit Sharing Plan (US)");
      map.put("20", "401K (US)");
      map.put("21", "Self-Directed IRA (US)");
      map.put("22", "403(b) (US)");
      map.put("23", "457 (US)");
      map.put("24", "Roth IRA (fund prototype) (US)");
      map.put("25", "Roth IRA (non-prototype) (US)");
      map.put("26", "Roth Conversion IRA (fund prototype) (US)");
      map.put("27", "Roth Conversion IRA (non-prototype) (US)");
      map.put("28", "Education IRA (fund prototype) (US)");
      map.put("29", "Education IRA (non-prototype) (US)");
      map.put("999", "Other");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("495", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("A", "Accepted");
      map.put("R", "Rejected");
      map.put("H", "Held");
      map.put("N", "Reminder");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("506", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("1", "Invalid/unacceptable Account Type");
      map.put("2", "Invalid/unacceptable Tax Exempt Type");
      map.put("3", "Invalid/unacceptable OwnershipType <517>");
      map.put("4", "Invalid/unacceptable NoRegistDtls <473>");
      map.put("5", "Invalid/unacceptable Reg Seq No");
      map.put("6", "Invalid/unacceptable RegistDetls <509>");
      map.put("7", "Invalid/unacceptable MailingDtls <474>");
      map.put("8", "Invalid/unacceptable MailingInst <482>");
      map.put("9", "Invalid/unacceptable Investor ID");
      map.put("10", "Invalid/unacceptable Investor ID Source");
      map.put("11", "Invalid/unacceptable DateOfBirth <486>");
      map.put("12", "Invalid/unacceptable InvestorCountryOfResidence <475>");
      map.put("13", "Invalid/unacceptable NoDistribInsts <510>");
      map.put("14", "Invalid/unacceptable DistribPercentage <512>");
      map.put("15", "Invalid/unacceptable DistribPaymentMethod <477>");
      map.put("16", "Invalid/unacceptable CashDistribAgentAcctName <502>");
      map.put("17", "Invalid/unacceptable CashDistribAgentCode <499>");
      map.put("18", "Invalid/unacceptable CashDistribAgentAcctNum <500>");
      map.put("99", "Other");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("507", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "New");
      map.put("1", "Replace");
      map.put("2", "Cancel");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("514", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("2", "Joint Trustees");
      map.put("J", "Joint Investors");
      map.put("T", "Tenants in Common");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("517", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("1", "Commission Amount (actual)");
      map.put("2", "Commission % (actual)");
      map.put("3", "Initial Charge Amount");
      map.put("4", "Initial Charge %");
      map.put("5", "Discount Amount");
      map.put("6", "Discount %");
      map.put("7", "Dilution Levy Amount");
      map.put("8", "Dilution Levy %");
      map.put("9", "Exit Charge Amount");
      map.put("10", "Exit Charge %");
      map.put("11", "Fund-based Renewal Commission % (a.k.a. Trail commission)");
      map.put("12", "Projected Fund Value (i.e. for investments intended to realise or exceed a specific future value)");
      map.put("13", "Fund-based Renewal Commission Amount (based on Order value)");
      map.put("14", "Fund-based Renewal Commission Amount (based on Projected Fund value)");
      map.put("15", "Net Settlement Amount");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("519", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("1", "Individual Investor");
      map.put("2", "Public Company");
      map.put("3", "Private Company");
      map.put("4", "Individual Trustee");
      map.put("5", "Company Trustee");
      map.put("6", "Pension Plan");
      map.put("7", "Custodian Under Gifts to Minors Act");
      map.put("8", "Trusts");
      map.put("9", "Fiduciaries");
      map.put("10", "Networking Sub-Account");
      map.put("11", "Non-Profit Organization");
      map.put("12", "Corporate Body");
      map.put("13", "Nominee");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("522", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("A", "Agency");
      map.put("G", "Proprietary");
      map.put("I", "Individual");
      map.put("P", "Principal");
      map.put("R", "Riskless Principal");
      map.put("W", "Agent for Other Member");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("528", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("1", "Program Trade");
      map.put("2", "Index Arbitrage");
      map.put("3", "Non-Index Arbitrage");
      map.put("4", "Competing Market Maker");
      map.put("5", "Acting as Market Maker or Specialist in the security");
      map.put("6", "Acting as Market Maker or Specialist in the underlying security of a derivative security");
      map.put("7", "Foreign Entity (of foreign government or regulatory jurisdiction)");
      map.put("8", "External Market Participant");
      map.put("9", "External Inter-connected Market Linkage");
      map.put("A", "Riskless Arbitrage");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("529", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("1", "Cancel orders for a security");
      map.put("2", "Cancel orders for an Underlying security");
      map.put("3", "Cancel orders for a Product <460>");
      map.put("4", "Cancel orders for a CFICode <461>");
      map.put("5", "Cancel orders for a SecurityType <167>");
      map.put("6", "Cancel orders for a trading session");
      map.put("7", "Cancel all orders");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("530", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "Cancel Request Rejected");
      map.put("1", "Cancel orders for a security");
      map.put("2", "Cancel orders for an Underlying security");
      map.put("3", "Cancel orders for a Product <460>");
      map.put("4", "Cancel orders for a CFICode <461>");
      map.put("5", "Cancel orders for a SecurityType <167>");
      map.put("6", "Cancel orders for a trading session");
      map.put("7", "Cancel all orders");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("531", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "Mass Cancel Not Supported");
      map.put("1", "Invalid or unknown Security");
      map.put("2", "Invalid or unknown Underlying security");
      map.put("3", "Invalid or unknown Product <460>");
      map.put("4", "Invalid or unknown CFICode <461>");
      map.put("5", "Invalid or unknown SecurityType <167>");
      map.put("6", "Invalid or unknown trading session");
      map.put("99", "Other");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("532", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "Indicative");
      map.put("1", "Tradeable");
      map.put("2", "Restricted Tradeable");
      map.put("3", "Counter (tradable)");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("537", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("1", "Cash");
      map.put("2", "Margin Open");
      map.put("3", "Margin Close");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("544", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("1", "Local (Exchange, ECN, ATS)");
      map.put("2", "National");
      map.put("3", "Global");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("546", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("1", "All or None");
      map.put("2", "Immediate or Cancel");
      map.put("3", "Partially and Remain active");
      map.put("4", "Same Price");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("549", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("1", "None");
      map.put("2", "Buy side is prioritized");
      map.put("3", "Sell side is prioritized");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("550", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "Symbol <55>");
      map.put("1", "SecurityType <167> and/or CFICode <461>");
      map.put("2", "Product <460>");
      map.put("3", "TradingSessionID <336>");
      map.put("4", "All Securities");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("559", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "Valid request");
      map.put("1", "Invalid or unsupported request");
      map.put("2", "No instruments found that match selection criteria");
      map.put("3", "Not authorized to retrieve instrument data");
      map.put("4", "<Instrument> data temporarily unavailable");
      map.put("5", "Request for instrument data not supported");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("560", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "Report by mulitleg security only");
      map.put("1", "Report by multileg security and by instrument legs");
      map.put("2", "Do not report status of multileg security");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("563", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("1", "Unknown or invalid TradingSessionID <336>");
      map.put("99", "Other");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("567", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "All trades");
      map.put("1", "Matched trades matching Criteria provided on request");
      map.put("2", "Unmatched trades that match criteria");
      map.put("3", "Unreported trades that match criteria");
      map.put("4", "Advisories that match criteria");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("569", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "compared, matched or affirmed");
      map.put("1", "uncompared, unmatched, or unaffirmed");
      map.put("2", "advisory or alert");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("573", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      // For NYSE and AMEX:
      map.put("A1", "Exact match on Trade Date <75>, Stock Symbol <55>, Quantity <53>, Price <44>, Trade Type <418>, and Special Trade Indicator plus four badges and execution time (within two-minute window)");
      map.put("A2", "Exact match on Trade Date, Stock Symbol, Quantity, Price, Trade Type, and Special Trade Indicator plus four badges");
      map.put("A3", "Exact match on Trade Date, Stock Symbol, Quantity, Price, Trade Type, and Special Trade Indicator plus two badges and execution time (within two-minute window)");
      map.put("A4", "Exact match on Trade Date, Stock Symbol, Quantity, Price, Trade Type, and Special Trade Indicator plus two badges");
      map.put("A5", "Exact match on Trade Date, Stock Symbol, Quantity, Price, Trade Type, and Special Trade Indicator plus execution time (within two-minute window)");
      map.put("AQ", "Compared records resulting from stamped advisories or specialist accepts/pair-offs");
      map.put("S1", "Summarized - Exact match on Trade Date <75>, Stock Symbol <55>, Quantity <53>, Price <44>, Trade Type <418>, and Special Trade Indicator plus four badges and execution time (within two-minute window)");
      map.put("S2", "Summarized - Exact match on Trade Date, Stock Symbol, Quantity, Price, Trade Type, and Special Trade Indicator plus four badges");
      map.put("S3", "Summarized - Exact match on Trade Date, Stock Symbol, Quantity, Price, Trade Type, and Special Trade Indicator plus two badges and execution time (within two-minute window)");
      map.put("S4", "Summarized - Exact match on Trade Date, Stock Symbol, Quantity, Price, Trade Type, and Special Trade Indicator plus two badges");
      map.put("S5", "Summarized - Exact match on Trade Date, Stock Symbol, Quantity, Price, Trade Type, and Special Trade Indicator plus execution time (within two-minute window)");
      map.put("M1", "Exact Match on Trade Date, Stock Symbol, Quantity, Price, Trade Type, and Special Trade Indicator minus badges and times");
      map.put("M2", "Summarized Match minus badges and times");
      map.put("MT", "OCS Locked In");

      // For NASDAQ
//      map.put("M1", "ACT M1 Match");
//      map.put("M2", "ACT M2 Match");
//      map.put("M3", "ACT Accepted Trade");
//      map.put("M4", "ACT Default Trade");
//      map.put("M5", "ACT Default After M2");
//      map.put("M6", "ACT M6 Match");
//      map.put("MT", "Non-ACT");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("574", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "process normally");
      map.put("1", "exclude from all netting");
      map.put("2", "bilateral netting only");
      map.put("3", "ex clearing");
      map.put("4", "special trade");
      map.put("5", "multilateral netting");
      map.put("6", "clear against central counterparty");
      map.put("7", "exclude from central counterparty");
      map.put("8", "Manual mode");
      map.put("9", "Automatic posting mode");
      map.put("10", "Automatic give-up mode");
      map.put("11", "Qualified Service Representative");
      map.put("12", "Customer Trade");
      map.put("13", "Self clearing");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("577", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("1", "Account is carried on customer Side of Books");
      map.put("2", "Account is carried on non-customer Side of books");
      map.put("3", "House Trader");
      map.put("4", "Floor Trader");
      map.put("6", "Account is carried on non-customer side of books and is cross margined");
      map.put("7", "Account is house trader and is cross margined");
      map.put("8", "Joint Backoffice Account");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("581", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("1", "Member trading for their own account");
      map.put("2", "Clearing Firm trading for its proprietary account");
      map.put("3", "Member trading for another member");
      map.put("4", "All other");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("582", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("1", "Status for orders for a security");
      map.put("2", "Status for orders for an Underlying security");
      map.put("3", "Status for orders for a Product <460>");
      map.put("4", "Status for orders for a CFICode <461>");
      map.put("5", "Status for orders for a SecurityType <167>");
      map.put("6", "Status for orders for a trading session");
      map.put("7", "Status for all orders");
      map.put("8", "Status for orders for a PartyID <448>");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("585", map);
    }

    {
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("587", MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.get("63"));
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("1", "Auto");
      map.put("2", "Speak first");
      map.put("3", "Accumulate");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("589", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "Each partial execution is a bookable unit");
      map.put("1", "Aggregate partial executions on this order, and book one trade per order");
      map.put("2", "Aggregate executions for this symbol, side, and settlement date");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("590", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "Pro-rata");
      map.put("1", "Do not pro-rata");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("591", map);
    }

    {
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("603", MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.get("22"));
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("606", MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.get("22"));
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("607", MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.get("460"));
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("609", MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.get("167"));
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("624", MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.get("54"));
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("1", "Calculated");
      map.put("2", "Preliminary");
      map.put("3", "Sellside Calculated Using Preliminary");
      map.put("4", "Sellside Calculated Without Preliminary");
      map.put("5", "Ready-To-Book");
      map.put("6", "Buyside Ready-To-Book");
      map.put("7", "Warehouse instruction");
      map.put("8", "Request to Intermediary");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("626", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("B", "CBOE Member");
      map.put("C", "Non-member and Customer");
      map.put("E", "Equity Member and Clearing Member");
      map.put("F", "Full and Associate Member trading for own account and as floor Brokers");
      map.put("H", "106.H and 106.J Firms");
      map.put("I", "GIM, IDEM and COM Membership Interest Holders");
      map.put("L", "Lessee and 106.F Employees");
      map.put("M", "All other ownership types");
      map.put("1", "1st year delegate trading for his own account");
      map.put("2", "2nd year delegate trading for his own account");
      map.put("3", "3rd year delegate trading for his own account");
      map.put("4", "4th year delegate trading for his own account");
      map.put("5", "5th year delegate trading for his own account");
      map.put("9", "6th year and beyond delegate trading for his own account");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("635", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "Priority Unchanged");
      map.put("1", "Lost Priority as result of order change");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("638", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "Pending Approval");
      map.put("1", "Approved");
      map.put("2", "Rejected");
      map.put("3", "Unauthorized request");
      map.put("4", "Invalid definition request");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("653", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("1", "Unknown symbol (Security)");
      map.put("2", "Exchange(Security) closed");
      map.put("3", "Quote Request <R> exceeds limit");
      map.put("4", "Too late to enter");
      map.put("5", "Invalid price");
      map.put("6", "Not authorized to request quote");
      map.put("7", "No match for inquiry");
      map.put("8", "No market for instrument");
      map.put("9", "No inventory");
      map.put("10", "Pass");
      map.put("99", "Other");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("658", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("1", "BIC");
      map.put("2", "SID code");
      map.put("3", "TFM (GSPTA)");
      map.put("4", "OMGEO (AlertID)");
      map.put("5", "DTCC code");
      map.put("99", "Other (custom or proprietary)");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("660", map);
    }

    {
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("661", MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.get("660"));
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("663", MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.get("423"));
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("1", "Received");
      map.put("2", "Mismatched account");
      map.put("3", "Missing settlement instructions");
      map.put("4", "Confirmed");
      map.put("5", "Request rejected");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("665", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "New");
      map.put("1", "Replace");
      map.put("2", "Cancel");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("666", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("1", "BookEntry");
      map.put("2", "Bearer");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("668", map);
    }

    {
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("674", MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.get("661"));
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("680", MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.get("663"));
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("682", MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.get("27"));
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("686", MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.get("423"));
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("1", "Par For Par");
      map.put("2", "Modified Duration");
      map.put("3", "Risk");
      map.put("4", "Proceeds");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("690", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("1", "percent");
      map.put("2", "per share");
      map.put("3", "fixed amount");
      map.put("4", "discount - percentage points below par");
      map.put("5", "premium - percentage points over par");
      map.put("6", "basis points relative to benchmark");
      map.put("7", "TED price");
      map.put("8", "TED yield");
      map.put("9", "Yield spread (swaps)");
      map.put("10", "Yield");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("692", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("1", "Hit/Lift");
      map.put("2", "Counter");
      map.put("3", "Expired");
      map.put("4", "Cover");
      map.put("5", "Done Away");
      map.put("6", "Pass");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("694", map);
    }

    {
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("695", MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.get("104"));
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("698", MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.get("423"));
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "Submitted");
      map.put("1", "Accepted");
      map.put("2", "Rejected");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("706", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("1", "Exercise");
      map.put("2", "Do Not Exercise");
      map.put("3", "Position Adjustment");
      map.put("4", "Position Change Submission/Margin Disposition");
      map.put("5", "Pledge");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("709", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("1", "New");
      map.put("2", "Replace");
      map.put("3", "Cancel");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("712", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "Process request as Margin Disposition");
      map.put("1", "Delta_plus");
      map.put("2", "Delta_minus");
      map.put("3", "Final");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("718", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "Accepted");
      map.put("1", "Accepted with Warnings");
      map.put("2", "Rejected");
      map.put("3", "Completed");
      map.put("4", "Completed with Warnings");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("722", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "Successful completion");
      map.put("1", "Rejected");
      map.put("99", "Other");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("723", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "Positions");
      map.put("1", "Trades");
      map.put("2", "Exercises");
      map.put("3", "Assignments");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("724", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "Inband");
      map.put("1", "Out-of-Band");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("725", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "Valid Request");
      map.put("1", "Invalid or unsupported Request");
      map.put("2", "No positions found that match criteria");
      map.put("3", "Not authorized to request positions");
      map.put("4", "Request for Position not supported");
      map.put("99", "Other");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("728", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "Completed");
      map.put("1", "Completed with Warnings");
      map.put("2", "Rejected");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("729", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("1", "Final");
      map.put("2", "Theoretical");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("731", map);
    }

    {
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("733", MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.get("731"));
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("735", MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.get("695"));
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("R", "Random");
      map.put("P", "ProRata");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("744", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("A", "Automatic");
      map.put("M", "Manual");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("747", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "Successful");
      map.put("1", "Invalid or unknown instrument");
      map.put("2", "Invalid type of trade requested");
      map.put("3", "Invalid parties");
      map.put("4", "Invalid Transport Type requested");
      map.put("5", "Invalid Destination requested");
      map.put("8", "TradeRequestType <569> not supported");
      map.put("9", "Unauthorized for Trade Capture Report Request <AD>");
      map.put("99", "Other");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("749", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "Accepted");
      map.put("1", "Completed");
      map.put("2", "Rejected");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("750", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "Successful");
      map.put("1", "Invalid party information");
      map.put("2", "Unknown instrument");
      map.put("3", "Unauthorized to report trades");
      map.put("4", "Invalid trade type");
      map.put("99", "Other");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("751", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("1", "Single Security");
      map.put("2", "Individual leg of a multileg security");
      map.put("3", "Multileg security");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("752", map);
    }

    {
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("759", MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.get("452"));
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("1", "Execution Time");
      map.put("2", "Time In");
      map.put("3", "Time Out");
      map.put("4", "Broker Receipt");
      map.put("5", "Broker Execution");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("770", map);
    }

    {
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("772", MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.get("666"));
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("1", "Status");
      map.put("2", "Confirmation");
      map.put("3", "Rejected");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("773", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("1", "Mismatched account");
      map.put("2", "Missing settlement instructions");
      map.put("99", "Other");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("774", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "Regular booking");
      map.put("1", "Contract For Difference");
      map.put("2", "Total return swap");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("775", map);
    }

    {
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("776", MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.get("88"));
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "use default instructions");
      map.put("1", "derive from parameters provided");
      map.put("2", "full details provided");
      map.put("3", "SSI db ids provided");
      map.put("4", "phone for instructions");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("780", map);
    }

    {
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("783", MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.get("447"));
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("784", MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.get("452"));
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("1", "Firm");
      map.put("2", "Person");
      map.put("3", "System");
      map.put("4", "Application");
      map.put("5", "Full legal name of firm");
      map.put("6", "Postal address");
      map.put("7", "Phone number");
      map.put("8", "Email address");
      map.put("9", "Contact name");
      map.put("10", "Securities account number");
      map.put("11", "Registration number");
      map.put("12", "Registered address");
      map.put("13", "Regulatory status");
      map.put("14", "Registration name");
      map.put("15", "Cash account number");
      map.put("16", "BIC");
      map.put("17", "CSD participant/member code");
      map.put("18", "Registered address");
      map.put("19", "Fund/account name");
      map.put("20", "Telex number");
      map.put("21", "Fax number");
      map.put("22", "Securities account name");
      map.put("23", "Cash account name");
      map.put("24", "Department");
      map.put("25", "Location / Desk");
      map.put("26", "Position Account Type");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("785", map);
    }

    {
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("786", MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.get("785"));
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("S", "securities");
      map.put("C", "cash");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("787", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("1", "Overnight");
      map.put("2", "Term");
      map.put("3", "Flexible");
      map.put("4", "Open");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("788", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "unable to process request");
      map.put("1", "unknown account");
      map.put("2", "no matching settlement instructions found");
      map.put("99", "other");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("792", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("3", "Sellside Calculated Using Preliminary");
      map.put("4", "Sellside Calculated Without Preliminary");
      map.put("5", "Warehouse recap");
      map.put("6", "Request to Intermediary");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("794", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("1", "Original details incomplete/incorrect");
      map.put("2", "Change in underlying order details");
      map.put("99", "Other");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("796", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("1", "Account is carried on customer Side of Books");
      map.put("2", "Account is carried on non-Customer Side of books");
      map.put("3", "House Trader");
      map.put("4", "Floor Trader");
      map.put("6", "Account is carried on non-customer side of books and is cross margined");
      map.put("7", "Account is house trader and is cross margined");
      map.put("8", "Joint Backoffice Account");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("798", map);
    }

    {
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("803", MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.get("785"));
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("805", MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.get("785"));
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("805", MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.get("785"));
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("1", "Pending Accept");
      map.put("2", "Pending Release");
      map.put("3", "Pending Reversal");
      map.put("4", "Accept");
      map.put("5", "Block Level Reject");
      map.put("6", "Account Level Reject");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("808", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "No action taken");
      map.put("1", "Queue flushed");
      map.put("2", "Overlay last");
      map.put("3", "End session");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("814", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "No action taken");
      map.put("1", "Queue flushed");
      map.put("2", "Overlay last");
      map.put("3", "End session");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("815", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "No Average Pricing");
      map.put("1", "Trade is part of an average price group identified by the TradeLinkID <820>");
      map.put("2", "Last Trade in the average price group identified by the TradeLinkID <820>");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("819", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "Allocation not required");
      map.put("1", "Allocation required");
      map.put("2", "Use allocation provided with the trade");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("826", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "Expire on trading session close");
      map.put("1", "Expire on trading session open");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("827", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "Regular Trade");
      map.put("1", "Block Trade");
      map.put("2", "Exchange for Physical");
      map.put("3", "Transfer");
      map.put("4", "Late Trade");
      map.put("5", "T Trade");
      map.put("6", "Weighted Average Price Trade");
      map.put("7", "Bunched Trade");
      map.put("8", "Late Bunched Trade");
      map.put("9", "Prior Reference Price Trade");
      map.put("10", "After Hours Trade");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("828", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "Floating");
      map.put("1", "Fixed");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("835", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "Price");
      map.put("1", "Basis Points");
      map.put("2", "Ticks");
      map.put("3", "Price Tier / Level");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("836", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "Or better");
      map.put("1", "Strict");
      map.put("2", "Or worse");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("837", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("1", "More aggressive");
      map.put("2", "More passive");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("838", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("1", "Local");
      map.put("2", "National");
      map.put("3", "Global");
      map.put("4", "National excluding local");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("840", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "Floating");
      map.put("1", "Fixed");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("841", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "Price");
      map.put("1", "Basis Points");
      map.put("2", "Ticks");
      map.put("3", "Price Tier / Level");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("842", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "Or better");
      map.put("1", "Strict");
      map.put("2", "Or worse");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("843", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("1", "More aggressive");
      map.put("2", "More passive");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("844", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("1", "Local");
      map.put("2", "National");
      map.put("3", "Global");
      map.put("4", "National excluding local");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("846", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("1", "VWAP");
      map.put("2", "Participate");
      map.put("3", "Mininize market impact");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("847", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("1", "Added Liquidity");
      map.put("2", "Removed Liquidity");
      map.put("3", "Liquidity Routed Out");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("851", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "Dealer Sold Short");
      map.put("1", "Dealer Sold Short Exempt");
      map.put("2", "Selling Customer Sold Short");
      map.put("3", "Selling Customer Sold Short Exempt");
      map.put("4", "Qualifed Service Representative (QSR) or Automatic Giveup (AGU) Contra Side Sold Short");
      map.put("5", "Qualifed Service Representative (QSR) or Automatic Giveup (AGU) Contra Side Sold Short Excempt");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("853", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("1", "Units");
      map.put("2", "Contracts");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("854", map);
    }

    {
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("855", MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.get("828"));
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "Submit");
      map.put("1", "Alleged");
      map.put("2", "Accept");
      map.put("3", "Decline");
      map.put("4", "Addendum");
      map.put("5", "No/Was");
      map.put("6", "Trade Report Cancel");
      map.put("7", "Locked In Trade Break");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("856", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "Not specified");
      map.put("1", "Explicit list provided");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("857", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("1", "Put");
      map.put("2", "Call");
      map.put("3", "Tender");
      map.put("4", "Sinking Fund Call");
      map.put("99", "Other");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("865", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("1", "Flat");
      map.put("2", "Zero coupon");
      map.put("3", "Interest bearing");
      map.put("4", "No periodic payments");
      map.put("5", "Variable rate");
      map.put("6", "Less fee for put");
      map.put("7", "Stepped coupon");
      map.put("8", "Coupon period");
      map.put("9", "When (and if) issued");
      map.put("10", "Original issue discount");
      map.put("11", "Callable, puttable");
      map.put("12", "Escrowed to Maturity");
      map.put("13", "Escrowed to redemption date - callable");
      map.put("14", "Prerefunded");
      map.put("15", "In default");
      map.put("16", "Unrated");
      map.put("17", "Taxable");
      map.put("18", "Indexed");
      map.put("19", "Subject to Alternative Minimum Tax");
      map.put("20", "Original issue discount price");
      map.put("21", "Callable below maturity value");
      map.put("22", "Callable without notice by mail to holder unless registered");
      map.put("99", "Text");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("871", map);
    }

    {
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("872", MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.get("871"));
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("1", "3(a)");
      map.put("2", "4");
      map.put("99", "Other");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("875", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "Absolute");
      map.put("1", "Per unit");
      map.put("2", "Percentage");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("891", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "Initial");
      map.put("1", "Scheduled");
      map.put("2", "Time Warning");
      map.put("3", "Margin Deficiency");
      map.put("4", "Margin Excess");
      map.put("5", "Forward Collateral Demand");
      map.put("6", "Event of default");
      map.put("7", "Adverse tax event");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("895", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "Trade Date");
      map.put("1", "GC Instrument");
      map.put("2", "CollateralInstrument");
      map.put("3", "Substitution Eligible");
      map.put("4", "Not Assigned");
      map.put("5", "Partially Assigned");
      map.put("6", "Fully Assigned");
      map.put("7", "Outstanding Trades (Today < end date)");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("896", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "New");
      map.put("1", "Replace");
      map.put("2", "Cancel");
      map.put("3", "Release");
      map.put("4", "Reverse");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("903", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "Received");
      map.put("1", "Accepted");
      map.put("2", "Declined");
      map.put("3", "Rejected");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("905", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "Unknown deal (order / trade)");
      map.put("1", "Unknown or invalid instrument");
      map.put("2", "Unauthorized transaction");
      map.put("3", "Insufficient collateral");
      map.put("4", "Invalid type of collateral");
      map.put("5", "Excessive substitution");
      map.put("99", "Other");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("906", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "Unassigned");
      map.put("1", "Partially Assigned");
      map.put("2", "Assignment Proposed");
      map.put("3", "Assigned (Accepted)");
      map.put("4", "Challenged");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("910", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "Versus Payment");
      map.put("1", "Free");
      map.put("2", "Tri-Party");
      map.put("3", "Hold In Custody");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("919", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("1", "LogOnUser");
      map.put("2", "LogOffUser");
      map.put("3", "ChangePasswordForUser");
      map.put("4", "Request Individual User Status");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("924", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("1", "Logged In");
      map.put("2", "Not Logged In");
      map.put("3", "User Not Recognised");
      map.put("4", "Password <554> Incorrect");
      map.put("5", "Password <554> Changed");
      map.put("6", "Other");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("926", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("1", "Connected");
      map.put("2", "Not connected - down expected up");
      map.put("3", "Not connected - down expected down");
      map.put("4", "In Process");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("928", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("1", "Snapshot");
      map.put("2", "Subscribe");
      map.put("4", "Stop subscribing");
      map.put("8", "Level of detail, then NoCompIDs <936> becomes required");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("935", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("1", "Full");
      map.put("2", "Incremental update");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("937", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "Accepted");
      map.put("1", "Rejected");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("939", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("1", "Received");
      map.put("2", "Confirm rejected");
      map.put("3", "Affirmed");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("940", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "Retain");
      map.put("1", "Add");
      map.put("2", "Remove");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("944", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "Accepted");
      map.put("1", "Accepted with Warnings");
      map.put("2", "Completed");
      map.put("3", "Completed with Warnings");
      map.put("4", "Rejected");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("945", map);
    }

    {
      Map<String, String> map = new LinkedHashMap<>();
      map.put("0", "Successful ");
      map.put("1", "Invalid or unknown instrument");
      map.put("2", "Invalid or unknown collateral type");
      map.put("3", "Invalid parties");
      map.put("4", "Invalid Transport Type requested");
      map.put("5", "Invalid Destination requested");
      map.put("6", "No collateral found for the trade specified");
      map.put("7", "No collateral found for the order specified");
      map.put("8", "Collateral Inquiry type not supported");
      map.put("9", "Unauthorized for collateral inquiry");
      map.put("99", "Other");
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("946", map);
    }

    {
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("950", MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.get("447"));
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("951", MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.get("452"));
      MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.put("954", MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.get("803"));
    }
  }

  public static String mapFieldCode(String fieldCode) {
    String fieldName = MAP_FIELD_CODE_TO_NAME.get(fieldCode);
    return fieldName != null ? fieldName : fieldCode;
  }

  public static String mapFieldValueCode(String fieldCode, String valueCode) {
    Map<String, String> mapValueCodeToName = MAP_FIELD_CODE_TO_MAP_VALUE_CODE_TO_NAME.get(fieldCode);
    if (mapValueCodeToName != null) {
      String valueName = mapValueCodeToName.get(valueCode);
      return valueName != null ? valueName : valueCode;
    }
    return valueCode;
  }
}
