# amxlog

## Installation

To create an installation Zip file:
```shell
./gradlew distZip
```

The generated distribution is in the directory `build/distributions`.

## Usage

```
USAGE: amxlog [options] [FILE]...
  Converts Assetmax log files for simplified analysis.

ARGUMENTS:
  FILE Assetmax log file to convert

OPTIONS:
  --fix
    Extracts FIX messages from the log files and converts them into a readable format.
  --call
    Extracts call request information (from client or API) and aggregates them into per-call information.
  --fix-keys KEYLIST
    In --fix mode a simplified message containing only specific FIX field values is printed.
    The KEYLIST is a list of FIX field keys (as code or mapped into name), separated by ','.
    Default: MsgType,OrdStatus,Side,SecurityID,OrderQty,Quantity,LastQty
  --call-params
    In --call mode show the parameters of the every call request.  --fix-protocol
    Prints the information about all FIX fields.  -?
  -h
  --help
    Prints this help text.
OUTPUT:
  Mode --fix:
    Every output line represents a single FIX message and consists of:
      - date
      - time
      - 'Fix'
      - '[' orderID ']'
      - '[' simplifiedMessage ']'
    and many repeated
      - '| ' field '=' value

    The `date` and `time` are in the same format as in the original log file.
    The `orderId` is useful as correlation ID and consists of the OrigClOrdID value, or ClOrdID value.

    The `simplifiedMessage` consists of the values of a limited list of important fields. 
    This output is useful to gain a quick overview over the FIX message.
    If not specified otherwise with --fix-keys these
    fields are: MsgType,OrdStatus,Side,SecurityID,OrderQty,Quantity,LastQty

    Fields and values are expanded according to FIX protocol.
    For example '35=8' is converted into 'MsgType(35)=Execution Report(8)'.
    If no field information is available, the original field or its original value is printed.

  Mode --call:
    Every output line represents a single call request and consists of:
      - date
      - time
      - 'Call'
      - URL
      - elapsedTime
      - params

    The `date` and `time` are in the same format as in the original log file.
    The `URL` is the URL of the call request.
    The `elapsedTime` is the elapsed time of the call execution in milliseconds.
    The `params` are the call request parameters, only printed if the --call-params options was specified.

EXAMPLES:
  Usually the Assetmax log files are initially converted using --fix or -- call modes.
  The resulting output file can then be further analyzed using `grep`, `sort` and viewed with `less -S`.

  Mode --fix:
    Convert the log file(s) using:
      amxlog --fix application*.log >fix.txt
    Find all messages (may be multiple orders) for a specific ISING CH0418792922:
      grep CH0025238863 fix.txt | less -S
    Output:
      11.08.21 13:54:38.535 Fix [XY22031792401] [New Order Single Buy CH0418792922 102] | BeginString(8)=FIX.4.4 ...
      11.08.21 13:56:46.010 Fix [XY22031792401] [Execution Report Rejected Buy CH0418792922] | BeginString(8)=FI ...
      11.08.21 14:04:01.583 Fix [XY22031742128] [New Order Single Sell CH0418792922 8] | BeginString(8)=FIX.4.4  ...
      11.08.21 14:04:16.209 Fix [XY22031742128] [Execution Report Pending New Sell CH0418792922 8] | BeginString ...
      11.08.21 14:06:36.331 Fix [XY22031742128] [Execution Report New Sell CH0418792922 8] | BeginString(8)=FIX. ...
      11.08.21 14:06:47.301 Fix [XY22031742128] [Execution Report Filled Sell CH0418792922 8 8] | BeginString(8) ...
      11.08.21 14:06:51.207 Fix [XY22031742128] [Allocation Instruction CH0418792922 8 Sell 8] | BeginString(8)= ...
    Find all messages for a specific correlation order ID (first column after 'Fix') XY22031792401:
      grep XY22031716682 fix.txt | less -S
    Output:
      11.08.21 13:54:38.535 Fix [XY22031792401] [New Order Single Buy CH0418792922 102] | BeginString(8)=FIX.4.4 ...
      11.08.21 13:56:46.010 Fix [XY22031792401] [Execution Report Rejected Buy CH0418792922] | BeginString(8)=FI ...

  Mode --call:
    Convert the log file(s) using:
      amxlog --call application*.log >call.txt
    or:
      amxlog --call --call-params application*.log >call.txt
    Find all calls of 'pf-operation/add-op-update-breaches' sorted by descending elapsed time:
      grep 'pf-operation/add-op-update-breaches' call.txt | sort -k 5 -n -r
```